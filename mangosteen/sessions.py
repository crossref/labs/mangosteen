import json
import logging
from collections import defaultdict
import streamlit as st

from mangosteen.data import (
    member_id_to_name_map,
    member_name_to_id_map,
    member_content_type_keys,
)
from mangosteen.settings import DEFAULT_SHARED_STATE, DEFAULT_STATE, LANGUAGES

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


##############################################################################
## Utility functions
##############################################################################


def session_state_debug(debug_vars):
    return {key: val for key, val in st.session_state.items() if key in debug_vars}


def session_state_debug_vars():
    return session_state_debug(DEFAULT_STATE.keys())


def shared_session_state_debug_variables():
    return session_state_debug(DEFAULT_SHARED_STATE.keys())


def cast_param_value(param, value):
    """Cast param value to correct type"""
    # Streamlit treats all params as lists, even if they are single values
    # So we need to convert them back to single values when appropriate
    # we use the values in DEFAULT_SHARED_STATE to determine if a param
    # should be a list or a single value
    truthy_values = {"true", "1", "t", "y", "yes", "yeah", "yup", "certainly", "uh-huh"}

    if isinstance(DEFAULT_SHARED_STATE[param], list):
        return value
    if isinstance(DEFAULT_SHARED_STATE[param], bool):
        return value[0].lower() in truthy_values

    return f"{value[0]}"


## Never touch state directly, always use these functions


def set_global_session_state(key, value):
    st.session_state[key] = value


def get_global_session_state(key, default=None):
    return st.session_state.get(key, default)


def set_shared_session_state(key, value):
    assert key in DEFAULT_SHARED_STATE
    set_global_session_state(key, value)


def get_shared_session_state(key, default=None):
    assert key in DEFAULT_SHARED_STATE
    return get_global_session_state(key, default)


# NB that streamlit session state is weird in that, if a widget is not
# used, it will purged from session state. This occurs frequently in the case
# of multi-page apps, where a widget is used on one page, but not on another.
# or in apps that show/hide widgets based on user input. These functions
# are designed to ensure that the session state is consistent across pages
# they work by shadowing the ephemeral widget key (e.g. _color) with the
# persistent session state key (e.g. color), you can then use the restore_widget_value
# to restore the widget value from the session state, and persist_widget_value to
# save the widget value to the session state. This ensures that the session state
# is always consistent with the widgets, even when the ephemeral widget keys are purged.


def persist_widget_value(key):
    st.session_state[key] = st.session_state[f"_{key}"]


def restore_widget_value(key):
    st.session_state[f"_{key}"] = st.session_state[key]


##############################################################################
## Setting initial state values and storing/restoring shared state from params
##############################################################################


def state_to_params():
    """Copy session state to URL"""
    for k, v in st.session_state.items():
        if k in DEFAULT_SHARED_STATE:
            st.query_params[k] = v


def params_to_state():
    for k in st.query_params:
        if k in DEFAULT_SHARED_STATE:
            st.session_state[k] = cast_param_value(k, st.query_params.get_all(k))


def new_session():
    logger.info("New session")
    st.session_state.update(DEFAULT_STATE)
    st.session_state.update(DEFAULT_SHARED_STATE)


##############################################################################
## Dealing with language and locale
##############################################################################


def currently_selected_language_locale():
    return LANGUAGES[get_shared_session_state("language")]["locale"]


##############################################################################
## Dealing with members
##############################################################################


def all_member_names():
    return list(member_name_to_id_map().keys())


def currently_selected_member_ids():
    id_strs = get_shared_session_state("selected-member-ids", [])
    return [int(id) for id in id_strs]


def member_id_to_name(id):
    return member_id_to_name_map()[id]


def member_name_to_id(name):
    return member_name_to_id_map()[name]


def currently_selected_member_names():
    return [member_id_to_name(id) for id in currently_selected_member_ids()]


##############################################################################
## Dealing with periods
##############################################################################


def currently_selected_period_id():
    return "current"


##############################################################################
## Dealing with content types
##############################################################################


def common_member_content_type_keys(selected_member_ids: list):
    ## Here
    common_type_counts = defaultdict(int)
    for member_id in selected_member_ids:
        member_types = member_content_type_keys(member_id)
        for types in member_types.values():
            for content_type, count in types.items():
                common_type_counts[content_type] += count
    return [
        k
        for k, v in sorted(
            common_type_counts.items(), key=lambda item: item[1], reverse=True
        )
    ] + ["everything-else"]


def content_type_names():
    return list(st.session_state.content_type_name_to_id_map.keys())


def content_type_keys():
    return list(st.session_state.content_type_id_to_name_map.keys())


def content_type_id_to_name(id):
    return st.session_state.content_type_id_to_name_map[id]


# def content_type_id_to_color(id):
#     if id == "everything-else":
#         # return hot-pink for everything else
#         return "#FF00FF"

#     return st.session_state.content_type_id_to_color_map[id]


def content_type_name_to_id(name):
    return st.session_state.content_type_name_to_id_map[name]


def currently_selected_type_id():
    return get_global_session_state("selected-content-type", "journal-article")


# def showing_title_detail():
#     return False


# def showing_example_links():
#     return False


# def foo():
#     pass

## Deal with download page


def cancel_download_examples():
    set_global_session_state("download-example", False)
