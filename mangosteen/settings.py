import streamlit as st

CODE_NAME = "mangosteen"
SERVICE_NAME = "Mangosteen"
PLUGINS_ENABLED_DIR = "plugins_enabled"


REPORTS_PAGE = "Reports"
REPORTS_PAGE_ICON = "📊"

DOWNLOAD_EXAMPLES_PAGE = "Download Examples"
DOWNLOAD_EXAMPLES_PAGE_ICON = "📥"

LANGUAGES = {
    "English": {"locale": "en-US", "flag": "🇺🇸"},
    "French": {"locale": "fr", "flag": "🇫🇷"},
    "German": {"locale": "de", "flag": "🇩🇪"},
    "Indonesian": {"locale": "id", "flag": "🇮🇩"},
}

NO_TITLE_SELECTION_LABEL = "All Titles"


PERIODS = ["all", "current", "backfile"]


DEFAULT_STATE = {
    "selected-member-names": [],
    "_language": "English",
    "inited": True,
    "api-url": None,
    "data-key": None,
    "download-example": None,
}


DEFAULT_SHARED_STATE = {
    "language": "English",
    "selected-member-ids": [],
    "selected-period": "current",
    "selected-content-type": "journal-article",
    "selected-title": NO_TITLE_SELECTION_LABEL,
    "use-tabs": False,
}

SUPPORTS_TITLE_DETAIL = ["journal-article", "book"]


MISSING_ITEM_FILTERS = {
    "abstracts": {"filter": {"has-abstract": "f"}},
    "affiliations": {"filter": {"has-affiliation": "f"}},
    "award-numbers": {"filter": {"has-award": "f"}},
    "orcids": {"filter": {"has-orcid": "f"}},
    "licenses": {"filter": {"has-license": "f"}},
    "references": {"filter": {"has-references": "f"}},
    "funders": {"filter": {"has-funder": "f"}},
    "similarity-checking": {"filter": {"has-full-text": "f"}},
    "ror-ids": {"filter": {"has-ror-id": "f"}},
    "update-policies": {"filter": {"has-update-policy": "f"}},
    "resource-links": {"filter": {"has-full-text": "f"}},
    # Descriptions is not showing as a valid filter on members route even though in swagger docs
    # 'descriptions': {'filter' :'has-description:f'}
}

API_URI = "https://api.crossref.org"
HEADERS = {
    "User-Agent": f"{CODE_NAME} (https://github.com/CrossRef/mangosteen); {SERVICE_NAME}; mailto:labs@crossref.org"
}
