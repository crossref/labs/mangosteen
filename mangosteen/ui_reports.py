import logging

import streamlit as st
from streamlit_extras.no_default_selectbox import selectbox

from mangosteen.data import load_member_data, load_title_data
from mangosteen.l10n import _
from mangosteen.metrics import Metrics
from mangosteen.plugins import enabled_plugins_info
from mangosteen.sessions import (
    content_type_id_to_name,
    content_type_name_to_id,
    content_type_names,
    currently_selected_member_ids,
    get_global_session_state,
    get_shared_session_state,
    set_global_session_state,
    set_shared_session_state,
)
from mangosteen.settings import NO_TITLE_SELECTION_LABEL, SUPPORTS_TITLE_DETAIL

METRICS = Metrics()


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def content_type_changed(plugin_index: int):
    session_state_key = f"selected_content_type_{plugin_index}"
    content_name = get_global_session_state(session_state_key)
    set_global_session_state("selected-content-type-name", content_name)
    content_type_id = content_type_name_to_id(content_name)
    set_shared_session_state("selected-content-type", content_type_id)


def period_changed(plugin_index: int):
    logger.info(f"period_changed for plugin: {plugin_index}")
    session_state_key = f"selected_period_{plugin_index}"
    period_name = get_global_session_state(session_state_key)
    set_global_session_state("selected-period", period_name)
    set_shared_session_state("selected-period", period_name)
    logger.info(f">>>> period_name: {period_name}")


def display_period_filter(index: int):
    period_name = get_shared_session_state("selected-period", "all")
    set_global_session_state(f"selected_period_{index}", period_name)
    st.selectbox(
        _("period-filter-label"),
        ["all", "current", "backfile"],
        index=0,
        help=_("period-filter-help"),
        key=f"selected_period_{index}",
        on_change=period_changed,
        kwargs={"plugin_index": index},
        format_func=lambda x: _(f"period-{x}-label"),
    )


def display_content_type_filter(index: int):
    content_type_id = get_shared_session_state("selected-content-type", None)
    content_type_name = content_type_id_to_name(content_type_id)
    set_global_session_state(f"selected_content_type_{index}", content_type_name)
    st.selectbox(
        _("content-type-filter-label"),
        content_type_names(),
        index=0,
        help=_("content-type-filter-help"),
        key=f"selected_content_type_{index}",
        on_change=content_type_changed,
        kwargs={"plugin_index": index},
    )


def title_filter_enabled():
    return (
        len(get_shared_session_state("selected-member-ids")) == 1
        and get_shared_session_state("selected-content-type") in SUPPORTS_TITLE_DETAIL
    )


def selected_title_changed():
    METRICS.journal_title_selected_counter.inc()


def display_title_filter(index: int):
    selected_member_id = currently_selected_member_ids()[0]
    df = load_title_data()
    titles = sorted(df[df["member-id"] == selected_member_id]["title"].to_list())
    selectbox(
        _("title-filter-label"),
        titles,
        key="selected-title",
        help=_("title-filter-help"),
        no_selection_label=NO_TITLE_SELECTION_LABEL,
        on_change=selected_title_changed,
    )


def display_content_filter(index: int):
    row1 = st.columns(4)
    with row1[0]:
        display_period_filter(index=index)
    with row1[1]:
        display_content_type_filter(index=index)
    row2 = st.columns(4)
    with row2[0]:
        st.caption("time period text")
    row3 = st.columns(2)
    with row3[0]:
        if title_filter_enabled():
            display_title_filter(index=index)
    st.divider()


def display_plugin_metadata(plugin):
    st.markdown(
        f'<div class="cr-plugin-header">{plugin.title()}</div>',
        unsafe_allow_html=True,
    )
    st.markdown(
        f'<div class="cr-plugin-description">{plugin.description()}</div>',
        unsafe_allow_html=True,
    )
    st.markdown(
        f'<div class="cr-plugin-version">V: {plugin.version()}</div>',
        unsafe_allow_html=True,
    )


def desired_container_type(titles):
    use_tabs = get_shared_session_state("use-tabs")
    return (
        list(st.tabs(titles))
        if use_tabs
        else [st.container(border=True) for _ in titles]
    )


def display_reports():
    plugins_info = enabled_plugins_info()
    titles = [item["plugin"].title() for item in plugins_info]

    containers = desired_container_type(titles)

    for index, container in enumerate(containers):
        plugin = plugins_info[index]["plugin"]
        with container:
            display_plugin_metadata(plugin)
            if "show_content_type_filter" in plugins_info[index]["hooks"]:
                logger.info("show content type filter")
                display_content_filter(index=index)
            else:
                logger.info("hide content type filter")
            plugin.display_data(
                data=load_member_data(),
            )
