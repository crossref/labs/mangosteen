import logging
import importlib
import pluggy
import sys
from mangosteen.hookspecs import ReportHooks
from pathlib import Path
import os
from glob import glob

from mangosteen.settings import PLUGINS_ENABLED_DIR, CODE_NAME

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

pm = pluggy.PluginManager(CODE_NAME)
pm.add_hookspecs(ReportHooks)


def extract_order_from_module_name(plugin_name: str) -> int:
    """Extract the order from the plugin name
    e.g. if the plugin name is plugins_enabled.03_coverage.coverage, return 03
    # if order not included in the module name, make it last in the list by returning 999
    """
    if "_" not in plugin_name:
        return 999
    return int(plugin_name.split(".")[-2].split("_")[0])


def enabled_plugins_info() -> list[dict]:
    """Return useful information about enabled plugins"""
    plugins = []
    plugin_to_distinfo = dict(pm.list_plugin_distinfo())
    for plugin in pm.get_plugins():
        logger.debug(f"plugin: {plugin.__module__}")
        plugin_info = {
            "name": plugin.__class__,
            "order": extract_order_from_module_name(plugin.__module__),
            "hooks": [h.name for h in pm.get_hookcallers(plugin)],
            "plugin": plugin,
        }
        if distinfo := plugin_to_distinfo.get(plugin):
            logger.debug(f"distinfo: {distinfo}")
            sys.exit()
            plugin_info["version"] = distinfo.version
            plugin_info["name"] = distinfo.project_name
        plugins.append(plugin_info)
    return sorted(plugins, key=lambda p: p["order"])


def enabled_plugin_paths():
    """Return a list of paths to enabled plugins
    NB that we use glob to find all python files in the plugins_enabled directory
    and its subdirectories because glob works with symlinks and we want to be able to
    use symlinks to enable and disable plugins"""
    return [
        Path(filename)
        for filename in glob(f"{PLUGINS_ENABLED_DIR}/**/*.py", recursive=True)
        if "__" not in filename and "tests" not in filename
    ]


def path_to_module_name(path: Path) -> str:
    """Convert a path to a module name"""
    return str(path).replace(os.path.sep, ".").replace(".py", "")


def unload_plugins():
    """Unregister all plugins"""
    logger.info("Unloading plugins")
    for index, plugin in enumerate(pm.get_plugins()):
        logger.info(f"Unregistering plugin {index}: {plugin}")
        pm.unregister(plugin)


def reload_plugins():
    """Reload all plugins"""
    logger.info("Reloading plugins")
    unload_plugins()
    load_plugins()


def load_plugins():
    # list python files in the dynamic_plugins directory
    # import them, and register them with the plugin manager
    # NB: this assumes that the plugin class is named the same as the file
    # e.g. eat.py contains a class called Eat
    for plugin_path in enabled_plugin_paths():
        module_name = path_to_module_name(plugin_path)
        module = importlib.import_module(module_name)
        # make sure that we always get fresh version
        # TODO, maybe only do this in dev mode?
        module = importlib.reload(module)
        class_name = plugin_path.stem.title()
        plugin = getattr(module, class_name)
        # if pm.is_registered(plugin()):
        #     logger.info(f"Plugin {class_name} already registered, replacing it")
        #     pm.unregister(plugin())
        pm.register(plugin())


if not hasattr(sys, "_called_from_test"):
    # Only load plugins if not running tests
    pm.load_setuptools_entrypoints(CODE_NAME)
