from pluggy import HookimplMarker
from pluggy import HookspecMarker
from streamlit import container
from pandas import DataFrame

hookspec = HookspecMarker("mangosteen")
hookimpl = HookimplMarker("mangosteen")


class ReportHooks:
    """Hook specifications for mangosteen report sections"""

    @hookspec
    def title():
        """Return the title of the report"""

    @hookspec
    def description() -> str:
        """Return the description of the report"""

    @hookspec
    def version() -> str:
        """Return the version of the report"""

    @hookspec
    def show_content_type_filter() -> bool:
        """Display the content type filter"""

    @hookspec
    def load_data(source: str):
        """Load data

        :param source: where to get the data
        :return: a list of ingredients
        """

    @hookspec
    def display_data(data: DataFrame):
        """Show data
        :container a streamlit container
        """
