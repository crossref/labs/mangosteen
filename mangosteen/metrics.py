import streamlit as st
from prometheus_client import Gauge, Counter, Summary, start_http_server
from mangosteen.settings import CODE_NAME


PORT = 8001


@st.cache_resource
def get_metrics_server():
    return Metrics()


def singleton(cls):
    instances = {}

    def get_instance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]

    return get_instance


# @singleton
@st.cache_resource
class Metrics:
    def __init__(self):
        # check to see if PORT is already in use

        # import socket

        # sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # result = sock.connect_ex(('127.0.0.1', PORT))
        # if result == 0:
        #     print("Port is in use")
        # sock.close()

        start_http_server(PORT)

        self.REQUEST_TIME = Summary("some_summary", "Time spent in processing request")

        self.new_session_counter = Counter(
            f"{CODE_NAME}_new_session_counter", "Number of new streamlit sessions"
        )
        self.existing_session_counter = Counter(
            f"{CODE_NAME}_existing_session_counter",
            "Number of page refreshes for existing streamlit sessions",
        )
        self.language_selection_counter = Counter(
            f"{CODE_NAME}_language_selection_counter",
            "How many times has each language been selected?",
            ["language"],
        )
        self.member_selection_counter = Counter(
            f"{CODE_NAME}_member_selection_counter",
            "How many times has each member been selected?",
            ["member"],
        )
        self.number_of_members_selected_counter = Counter(
            f"{CODE_NAME}_number_of_members_selected_counter",
            "How many members to users select to compare?",
            ["selected_member_count"],
        )

        self.journal_title_selected_counter = Counter(
            f"{CODE_NAME}_journal_title_selected_counter",
            "How many times do users drill-down to title level?",
        )

        self.quarterly_report_downloaded_counter = Counter(
            f"{CODE_NAME}_quarterly_report_downloaded_counter",
            "How many times do users download quarterly reports?",
        )
