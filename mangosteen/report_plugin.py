import pluggy
from mangosteen.l10n import _

hookimpl = pluggy.HookimplMarker("mangosteen")


class ReportPlugin:
    @hookimpl
    def about():
        return "There is no 'about' text for this report plugin"

    @hookimpl
    def _(txt: str):
        return _(txt)
