import logging

import streamlit as st

from mangosteen.data import (
    load_member_data,
    member_id_to_name_map,
)
from mangosteen.l10n import _
from mangosteen.sessions import (
    currently_selected_member_ids,
    currently_selected_type_id,
)
from mangosteen.ui_data_point import display_data_point

LABEL_COLUMN_COUNT = 1
ROW_LABEL_COL_POS = 0
LOGO_ROW_POS = 1
COLUMN_COUNT_OFFSET = 1  # How many non-progress columns there are?

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def data_label(data_point_config):
    return data_point_config.get("label", "")


def display_row_label(data_point_config):
    label = data_label(data_point_config)
    row_help = data_point_config.get("help", "")
    st.markdown(
        f'<span class="cr-gridify-row-label">{label}</span>',
        unsafe_allow_html=True,
        help=row_help,
    )


def display_col_header(dp):
    st.markdown(
        f'<span class="cr-gridify-column-label">{dp}</span>',
        unsafe_allow_html=True,
    )


def display_crossref_avg_column(row_index, data_key, all_data, data_point_config):
    if row_index == ROW_LABEL_COL_POS:
        display_col_header(_("crossref-average"))
    elif row_index == LOGO_ROW_POS:
        st.image("https://logo.clearbit.com/crossref.org")
    elif row_index == 2:
        st.write(
            f'<p class="cr-gridify-cell">{_("all-member-ids")}</p>',
            unsafe_allow_html=True,
        )
    else:
        data_point = all_data[data_key].mean()
        display_data_point(st, data_point, dp_config=data_point_config)


def display_side_by_side(data, row_keys, column_config, show_cr_avg=True):
    selected_member_ids = currently_selected_member_ids()
    COLUMN_COUNT = len(selected_member_ids) + (1 if show_cr_avg else 0)

    for row_index, data_key in enumerate(row_keys):
        data_point_config = column_config.get(data_key, None)
        for column_index, column in enumerate(
            st.columns(1 + COLUMN_COUNT)  # Directly using 1 for LABEL_COLUMN_COUNT
        ):
            with column.container(border=False):
                if column_index == 0:  # Assuming ROW_LABEL_COL_POS is meant to be 0
                    display_row_label(data_point_config)
                elif column_index <= len(selected_member_ids):
                    member_id = selected_member_ids[column_index - 1]
                    data_point = data[row_index][column_index - 1]
                    if row_index == 0:  # Assuming ROW_LABEL_COL_POS is meant to be 0
                        display_col_header(data_point)
                    else:
                        display_data_point(
                            st,
                            data_point,
                            dp_config=data_point_config,
                            example_text={
                                "content-type": currently_selected_type_id(),
                                "data-label": data_label(data_point_config),
                                "filter-key": data_key,
                                "member-id": member_id,
                                "member-name": member_id_to_name_map()[member_id],
                            },
                        )
                elif show_cr_avg:
                    display_crossref_avg_column(
                        row_index, data_key, load_member_data(), data_point_config
                    )
