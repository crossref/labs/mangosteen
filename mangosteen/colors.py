""" Python colormaps  """

import json

import matplotlib.colors as mcolors


import numpy as np

CR_PRIMARY_RED = "#ef3340"
CR_PRIMARY_GREEN = "#3eb1c8"
CR_PRIMARY_YELLOW = "#ffc72c"
CR_PRIMARY_LT_GREY = "#d8d2c4"
CR_PRIMARY_DK_GREY = "#4f5858"

CR_SECONDARY_RED = "#a6192e"
CR_SECONDARY_GREEN = "#00ab84"
CR_SECONDARY_YELLOW = "#ffa300"
CR_SECONDARY_BLUE = "#005f83"
CR_SECONDARY_LT_GREY = "#a39382"

CR_MONO_1 = "#d9d9d9"
CR_MONO_2 = "#bfbfbf"
CR_MONO_3 = "#a6a6a6"
CR_MONO_4 = "#595959"
CR_MONO_5 = "#333333"

CR_PRIMARY_COLORS = {
    "CR_PRIMARY_RED": "#ef3340",
    "CR_PRIMARY_GREEN": "#3eb1c8",
    "CR_PRIMARY_YELLOW": "#ffc72c",
    "CR_PRIMARY_LT_GREY": "#d8d2c4",
    "CR_PRIMARY_DK_GREY": "#4f5858",
}


def hex_to_rgb(hex_color):
    """Convert a hex color to an RGB tuple."""
    hex_color = hex_color.lstrip("#")
    return tuple(int(hex_color[i : i + 2], 16) for i in (0, 2, 4))


def rgb_to_hex(rgb_color):
    """Convert an RGB tuple back to a hex color."""
    return "#" + "".join(f"{int(c):02x}" for c in rgb_color)


def interpolate_rgb(color1, color2, factor):
    """Interpolate between two RGB colors."""
    return tuple(color1[i] + (color2[i] - color1[i]) * factor for i in range(3))


def generate_gradient_palette(color_list, palette_length):
    # Convert hex colors to RGB
    colors_rgb = [mcolors.hex2color(color) for color in color_list]

    # Calculate the positions of the original colors in the final palette
    color_positions = np.linspace(0, palette_length - 1, len(color_list)).astype(int)

    # Initialize an array to hold the final palette
    palette = np.zeros((palette_length, 3))

    # Place the original colors in the correct positions in the palette
    for idx, pos in enumerate(color_positions):
        palette[pos] = colors_rgb[idx]

    # Interpolate the RGB values for the positions in between
    for i in range(3):  # for R, G, B channels
        # channel_values = palette[:, i]
        known_positions = color_positions
        known_values = [color[i] for color in colors_rgb]
        interpolated_channel = np.interp(
            range(palette_length), known_positions, known_values
        )
        palette[:, i] = interpolated_channel

    # Convert back to hex and return
    return [mcolors.to_hex(color) for color in palette]


def generate_prioritized_palette(primary_colors=CR_PRIMARY_COLORS, palette_length=30):
    primary_color_values = list(primary_colors.values())
    gradient_palette = generate_gradient_palette(primary_color_values, palette_length)
    return primary_color_values + [
        color for color in gradient_palette if color not in primary_color_values
    ]


def get_types_palette():
    with open("data/type_color_palette.json") as f:
        return json.load(f)


def main():
    print(generate_prioritized_palette(CR_PRIMARY_COLORS, 30))


if __name__ == "__main__":
    main()
