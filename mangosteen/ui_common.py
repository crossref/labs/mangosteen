import streamlit as st


def insert_css():
    with open("static/app.css") as css:
        st.markdown(f"<style>{css.read()}</style>", unsafe_allow_html=True)
