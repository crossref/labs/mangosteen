import streamlit as st
import logging

from fluent.runtime import FluentLocalization, FluentResourceLoader

from mangosteen.settings import LANGUAGES
from mangosteen.sessions import (
    get_shared_session_state,
    currently_selected_language_locale,
)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

loader = FluentResourceLoader("l10n/{locale}")
locales = [lang["locale"] for lang in LANGUAGES.values()]
l10n = FluentLocalization(locales, ["main.ftl"], loader)


@st.cache_resource
def cached_key(k, locale):
    lang_order = lang_order = [locale, "en-US"]
    l10n = FluentLocalization(lang_order, ["main.ftl"], loader)
    return l10n.format_value(k)


# def _(k):
#     locale = get_shared_session_state("locale")
#     lang_order = [locale, "en-US"]
#     l10n = FluentLocalization(lang_order, ["main.ftl"], loader)
#     return l10n.format_value(k)


def _(k):
    # locale = get_shared_session_state("locale")
    locale = currently_selected_language_locale()
    return cached_key(k, locale)
