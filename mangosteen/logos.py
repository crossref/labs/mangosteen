import requests
from PIL import Image


def no_logo():
    return Image.open("static/no_logo.png")


def get_logo(domain):
    url = f"https://logo.clearbit.com/{domain}"
    try:
        response = requests.head(url)
        return url if response.status_code == 200 else no_logo()
    except:
        return no_logo()
