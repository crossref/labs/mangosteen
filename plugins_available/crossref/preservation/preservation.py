# from mangosteen.column_definitions import column_config
import streamlit as st
from pandas import DataFrame

from mangosteen.column_definitions import get_column_config
from mangosteen.data import load_member_data, extract_data
from mangosteen.hookspecs import hookimpl
from mangosteen.l10n import _
from mangosteen.sessions import (
    currently_selected_member_ids,
)
from mangosteen.side_by_side import display_side_by_side

PRESERVATION_COLUMNS = ["primary-name", "primary-resource-logo", "id", "preservation"]


class Preservation:
    @hookimpl
    def title(self):
        return "Preservation"

    @hookimpl
    def description(self):
        return "How good is this member at meeting their Crossref membership archiving obligations?"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def load_data(self, source: str):
        return f"Loading counts for {source}"

    @hookimpl
    def display_data(self, data: DataFrame):
        with st.spinner(_("loading-member-data")):
            data = load_member_data()

        selected_member_ids = currently_selected_member_ids()

        filtered_data = data[data["id"].isin(selected_member_ids)][PRESERVATION_COLUMNS]
        column_config = get_column_config(filtered_data)

        data = extract_data(filtered_data, PRESERVATION_COLUMNS)

        display_side_by_side(data, PRESERVATION_COLUMNS, column_config, False)
