# from mangosteen.column_definitions import column_config
import streamlit as st
from pandas import DataFrame
from streamlit_extras.dataframe_explorer import dataframe_explorer
from mangosteen.sessions import currently_selected_member_ids
from mangosteen.data import load_member_data
from mangosteen.hookspecs import hookimpl
from mangosteen.l10n import _


class Explorer:
    @hookimpl
    def title(self):
        return "Data Explorer"

    @hookimpl
    def description(self):
        return "Filter and browse data"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def load_data(self, source: str):
        return f"Loading counts for {source}"

    @hookimpl
    def display_data(self, data: DataFrame):
        with st.spinner(_("loading-member-data")):
            data = load_member_data()
        selected_member_ids = currently_selected_member_ids()
        filtered_data = data[data["id"].isin(selected_member_ids)]

        # filtered_df = dataframe_explorer(data, case=False)
        # get the name of the "prefix-names" column and convert it into json
        prefix_names = filtered_data["prefix-names"].to_json()
        # st.dataframe(
        #     filtered_data[["prefix-names"]], use_container_width=True
        # )
        st.json(prefix_names)
        # prefix_json = filtered_data["prefixes"]
        # st.json(prefix_json)
