import logging
import requests
import json
import typer
from pathlib import Path
from mangosteen.colors import generate_prioritized_palette, CR_PRIMARY_COLORS

TYPES_COUNT_URL_TEMPLATE = "https://api.crossref.org/works?filter=type:{type_id}&rows=0&mailto=labs@crossref.org"


def get_type_count(type_id):
    url = TYPES_COUNT_URL_TEMPLATE.format(type_id=type_id)
    logging.info(f"Getting count for {type_id}")
    logging.info(f"{url}")
    try:
        response = requests.get(url)
        response.raise_for_status()
        return response.json()["message"]["total-results"]
    except requests.exceptions.RequestException as e:
        logging.error(f"Error getting count for  {type_id}: {e}")
        return 0


def add_type_counts(types):
    for type in types:
        type["count"] = get_type_count(type["id"])
    # return ordereddict of types based on count
    return sorted(types, key=lambda x: x["count"], reverse=True)


def add_color_palette_mapping(types):
    prioritized_palette = generate_prioritized_palette(CR_PRIMARY_COLORS, len(types))
    for i, type in enumerate(types):
        type["color"] = prioritized_palette[i]
    return types


def summarize_types(fn):
    with open(fn, "r") as f:
        data = json.load(f)
        types = data["message"]["items"]  # [:6]
    return add_color_palette_mapping(add_type_counts(types))


def prepare_types_data(fn):
    types = summarize_types(fn)
    palette = {type["id"]: type["color"] for type in types}
    palette["everything-else"] = "#000000"
    return palette


def main(fn: Path = typer.Argument(...)):
    logging.basicConfig(level=logging.INFO)
    palette = prepare_types_data(fn)
    print(json.dumps(palette, indent=2))
    logging.info("Done")


if __name__ == "__main__":
    typer.run(main)
