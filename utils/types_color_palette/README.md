# Generate color palette for displaying content types in charts

This will generate a color palette that:

- Prioritizes the the primary Crossref palette for the most common conent types 
- Uses colors interpolated from the standard Crossref palette for all other types. It orders these by the popularity of the type.
- Uses 'black', `"#000000"` (or whatever you want to set it to) for the synthetic, rollup 'everything-else' type


Example usage:
```
python generate_types_color_palette.py types.json > ../../data/type_color_
palette.json
```