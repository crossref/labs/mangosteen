#!/usr/bin/env bash
fly deploy --local-only --no-cache --build-arg COMMIT=$(git rev-parse --short HEAD)