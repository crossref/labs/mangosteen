## To run translations

export GOOGLE_APPLICATION_CREDENTIALS="~/google-translate-service.json"
./bootstrap-translations.sh

echo $(git rev-parse HEAD) > COMMIT

## Download logic

ROWS_PER_REQUEST = 500
MAX_DOWNLOAD_SIZE = 5000

if missing_metadata_count > MAX_DOWNLOAD_SIZE then
metadata_url = sample_url
metadata_func = get_sample_func
else
metadata_url = first_cursor_url
metadata_func = get_metadata_func

## Mapping colors

domain = [5, 8, 10, 12, 25]
range\_ = ['#9cc8e2', '#9cc8e2', 'red', '#5ba3cf', '#125ca4']

alt.Chart(cars).mark*point().encode(
x='Horsepower',
y='Miles_per_Gallon',
color=alt.Color('Acceleration').scale(domain=domain, range=range*)
)

Write a Python script that, given two parameters:

- color_list
- palette_length

returns a palette_length-long list of hex colors that are a gradient of evenly spaced colors between (and include) the hex colors in color_list. So, for example, if I give a list of 10 colors and a palette_length of 30, it should return a gradient list of 30 evenly-spaced hex colors including the colors that were given in the parameter.


## Steps to add a new column to overview (or another plugin)

- Make sure column is collected by prepare_data
- Add column to OVERVIEW_COLUMNS list
- Add column to COLUMN_TYPE_TO_DISPLAY_FUNCTION in ui_data_point.py
- Update column_definitions.py *if* it needs special display logic (e.g. not text, number, boolean)