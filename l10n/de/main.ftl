my-first-string = Englisch!

loading-data = Daten werden geladen...

splash-screen-heading = Begrüßungsbildschirm

results-screen-heading = Berichte

member-selector-heading = Mitgliederauswahl

member-selector-label = Mitglied auswählen

member-selector-placeholder = Wähle eine Option

member-selector-help = Wählen Sie die Crossref-Mitglieder aus, für die Sie Daten zur Datenbeteiligung anzeigen möchten

comment = Neu 

content-filter-heading = Inhaltsfilter

period-filter-label = Zeitraum

period-filter-help = Wählen Sie den Veröffentlichungszeitraum aus, für den Sie Abdeckungsdaten anzeigen möchten. „Aktuelle“ Werke sind diejenigen, die im aktuellen Kalenderjahr oder in den beiden vorangegangenen Kalenderjahren veröffentlicht wurden.

period-all-label = Alle

period-all-help = Inhalte können jederzeit veröffentlicht werden, einschließlich aktueller und veralteter Inhalte

period-current-label = Aktuell

period-current-help = Inhalte, die im aktuellen Kalenderjahr oder in den beiden vorangegangenen Kalenderjahren veröffentlicht wurden

period-backfile-label = Backfile

period-back-file-help = Inhalte, die vor mehr als zwei Kalenderjahren veröffentlicht wurden

content-type-filter-label = Inhaltstyp

content-type-filter-help = Wählen Sie den Inhaltstyp aus, für den Sie Abdeckungsdaten anzeigen möchten

primary-name-label = Mitgliedsname

primary-name-help = Der primäre Name der Mitgliedsorganisation

primary-resource-logo-label = Logo

primary-resource-logo-help = Das Logo des Mitglieds, wie es von Clearbit abgerufen wurde und auf der primären Ressourcendomäne des Mitglieds basiert.

id-label = Mitgliedsnummer

id-help = Die eindeutige ID des Mitglieds in der Crossref-REST-API (https://www.crossref.org/documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-api/)

account-type-label = Konto Typ

non-profit-label = Gemeinnützig

date-joined-label = Datum des Beitritts

annual-fee-label = Jahresgebühr

active-label = Aktiv

board-status-label = Vorstandsstatus

board-status-help = Ist das Mitglied ein aktuelles oder ehemaliges Mitglied des Crossref-Vorstands (https://www.crossref.org/board-and-governance/)?

current-dois-label = Aktuelle DOIs

current-dois-help = Inhalte, die im aktuellen Kalenderjahr oder in den beiden vorangegangenen Kalenderjahren veröffentlicht wurden

backfile-dois-label = Backfile-DOIs

backfile-dois-help = Inhalte, die vor mehr als zwei Kalenderjahren veröffentlicht wurden

total-dois-label = Gesamt-DOIs

primary-resource-domain-label = Primäre Ressourcendomäne

primary-resource-domain-help = Die Domäne, in der die meisten von diesem Mitglied registrierten Inhalte gehostet werden

affiliations-current-label = Zugehörigkeiten (aktuell)

similarity-checking-current-label = Ähnlichkeitsprüfung (aktuell)

descriptions-current-label = Beschreibungen (aktuell)

ror-ids-current-label = ROR-IDs (aktuell)

funders-backfile-label = Geldgeber (backfile)

licenses-backfile-label = Lizenzen (Backfile)

funders-current-label = Geldgeber (aktuell)

affiliations-backfile-label = Zugehörigkeiten (aktuell)

resource-links-backfile-label = Ressourcenlinks (Backfile)

orcids-backfile-label = ORCID-iDs (Backfile)

update-policies-current-label = Update-Richtlinien (aktuell)

ror-ids-backfile-label = ROR-IDs (Backfile)

orcids-current-label = ORCID-iDs (aktuell)

similarity-checking-backfile-label = Ähnlichkeitsprüfung (Backfile)

references-backfile-label = Referenzen (backfile)

descriptions-backfile-label = Beschreibungen (Backfile)

award-numbers-backfile-label = Auszeichnungsnummern (Backfile)

update-policies-backfile-label = Update-Richtlinien (Backfile)

licenses-current-label = Lizenzen (aktuell)

award-numbers-current-label = Auszeichnungsnummern (aktuell)

abstracts-backfile-label = Abstracts (Backfile)

resource-links-current-label = Ressourcenlinks (aktuell)

abstracts-current-label = Abstracts (aktuell)

references-current-label = Referenzen (aktuell)

overall-coverage-label = Gesamtabdeckung

overall-impact-label = Gesamtauswirkung

earliest-publication-year-label = Frühestes Erscheinungsjahr

earliest-publication-year-help = Das früheste Kalenderjahr, in dem das Mitglied bei Crossref registrierte Inhalte veröffentlicht hat

latest-publication-year-label = Letztes Erscheinungsjahr

latest-publication-year-help = Das letzte Kalenderjahr, in dem das Mitglied bei Crossref registrierte Inhalte veröffentlicht hat

2010-total-label =
  2010-insgesamt
  2011-Gesamtlabel = 2011-Gesamt
  2012-Gesamtlabel = 2012-Gesamt
  2013-Gesamtlabel = 2013-Gesamt
  2014-Gesamtlabel = 2014-Gesamt
  2015-Gesamtlabel = 2015-Gesamt
  2016-Gesamtlabel = 2016-Gesamt
  2017-Gesamtlabel = 2017-Gesamt
  2018-Gesamtlabel = 2018-Gesamt
  2019-Gesamtlabel = 2019-Gesamt
  2020-Gesamtlabel = 2020-Gesamt
  2021-Gesamtlabel = 2021-Gesamt

avg-pct-change-label = Durchschnittliche prozentuale Änderung PA

display-name-label = Anzeigename

all-journal-article-abstracts-label = Zusammenfassungen

all-journal-article-abstracts-help = Der Prozentsatz des Inhalts, der die Zusammenfassung in den Metadaten enthält, wodurch weitere Einblicke in den Inhalt des Werks gewährt und es besser auffindbar gemacht wird. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

all-journal-article-affiliations-label = Zugehörigkeiten

all-journal-article-affiliations-help = Der Prozentsatz des Inhalts, der Zugehörigkeitsmetadaten für Mitwirkende enthält, sodass die Forschungsergebnisse von Institutionen nachverfolgt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

all-journal-article-award-numbers-label = Auszeichnungsnummern

all-journal-article-award-numbers-help = Der Prozentsatz der registrierten Inhalte, die mindestens eine Förderpreisnummer enthalten – eine von der Förderorganisation zugewiesene Nummer, um den spezifischen Förderbetrag (den Preis oder Zuschuss) zu identifizieren. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-descriptions-label = Beschreibungen

all-journal-article-descriptions-help = Der Prozentsatz der registrierten Inhalte, die eine Förder- oder Preisbeschreibung enthalten. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-funders-label = Geldgeber

all-journal-article-funders-help = Der Prozentsatz der registrierten Inhalte, die Metadaten über die Fördereinrichtung enthalten, die einen Zuschuss oder eine Auszeichnung gewährt. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-licenses-label = Lizenzen

all-journal-article-licenses-help = Der Prozentsatz der registrierten Inhalte, die URLs enthalten, die auf eine Lizenz verweisen, die die Bedingungen erläutert, unter denen Leser auf Inhalte zugreifen können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/license/

all-journal-article-orcids-label = ORCID-IDs

all-journal-article-orcids-help = Der Prozentsatz des Inhalts, der ORCID-iDs enthält. Diese dauerhaften Identifikatoren ermöglichen es Benutzern, die Arbeit eines Forschers genau zu identifizieren – selbst wenn dieser Forscher einen Namen mit jemand anderem teilt oder wenn er seinen Namen ändert. Erfahren Sie mehr: https://www.crossref.org/documentation/reports/participation-reports/#00198

all-journal-article-references-label = Verweise

all-journal-article-references-help = Der Prozentsatz der registrierten Inhalte, die Referenzlisten in den Metadaten enthalten, sodass Zitate zwischen Forschungsergebnissen verfolgt und gezählt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/references/

all-journal-article-resource-links-label = Ressourcenlinks

all-journal-article-resource-links-help = Der Prozentsatz der registrierten Inhalte, die Volltext-URLs in den Metadaten enthalten, um Forschern das einfache Auffinden von Inhalten für Text- und Data-Mining zu erleichtern. Erfahren Sie mehr: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

all-journal-article-ror-ids-label = RORs

all-journal-article-ror-ids-help = Der Prozentsatz des Inhalts, der Zugehörigkeitsmetadaten für Mitwirkende mit mindestens einer ROR-ID enthält, sodass Institutionen eindeutig identifiziert und ihre Forschungsergebnisse zurückverfolgt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

all-journal-article-similarity-checking-label = Ähnlichkeitsprüfung

all-journal-article-similarity-checking-help = Der Prozentsatz der registrierten Inhalte, die Volltextlinks für die Ähnlichkeitsprüfung enthalten, die es Mitgliedern ermöglichen, am Dienst teilzunehmen und Plagiate zu verhindern. Erfahren Sie mehr: https://www.crossref.org/documentation/similarity-check/participate/

all-journal-article-update-policies-label = Richtlinien aktualisieren

all-journal-article-update-policies-help = Der Prozentsatz der registrierten Inhalte, die den Crossmark-Dienst nutzen, der den Lesern einen schnellen und einfachen Zugriff auf den aktuellen Status eines Inhaltselements ermöglicht – unabhängig davon, ob dieser aktualisiert, korrigiert oder zurückgezogen wurde. Erfahren Sie mehr: https://www.crossref.org/services/crossmark/

current-journal-article-abstracts-help = Der Prozentsatz des Inhalts, der die Zusammenfassung in die Metadaten einbezieht, wodurch weitere Einblicke in den Inhalt des Werks gewährt und es besser auffindbar gemacht wird. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

current-journal-article-abstracts-label = Zusammenfassungen

current-journal-article-affiliations-label = Zugehörigkeiten

current-journal-article-affiliations-help = Der Prozentsatz des Inhalts, der Zugehörigkeitsmetadaten für Mitwirkende enthält, sodass die Forschungsergebnisse von Institutionen nachverfolgt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

current-journal-article-award-numbers-label = Auszeichnungen

current-journal-article-award-numbers-help = Der Prozentsatz der registrierten Inhalte, die mindestens eine Förderpreisnummer enthalten – eine von der Förderorganisation zugewiesene Nummer, um den spezifischen Förderbetrag (den Preis oder Zuschuss) zu identifizieren. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-descriptions-label = Beschreibungen

current-journal-article-descriptions-help = Der Prozentsatz der registrierten Inhalte, die eine Förder- oder Preisbeschreibung enthalten. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-funders-label = Geldgeber

current-journal-article-funders-help = Der Prozentsatz der registrierten Inhalte, die Metadaten über die Fördereinrichtung enthalten, die einen Zuschuss oder eine Auszeichnung gewährt. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-licenses-label = Lizenzen

current-journal-article-licenses-help = Der Prozentsatz der registrierten Inhalte, die URLs enthalten, die auf eine Lizenz verweisen, die die Bedingungen erläutert, unter denen Leser auf Inhalte zugreifen können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/license/

current-journal-article-orcids-label = ORCID-IDs

current-journal-article-orcids-help = Der Prozentsatz des Inhalts, der ORCID-iDs enthält. Diese dauerhaften Identifikatoren ermöglichen es Benutzern, die Arbeit eines Forschers genau zu identifizieren – selbst wenn dieser Forscher einen Namen mit jemand anderem teilt oder wenn er seinen Namen ändert. Erfahren Sie mehr: https://www.crossref.org/documentation/reports/participation-reports/#00198

current-journal-article-references-label = Verweise

current-journal-article-references-help = Der Prozentsatz der registrierten Inhalte, die Referenzlisten in den Metadaten enthalten, sodass Zitate zwischen Forschungsergebnissen verfolgt und gezählt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/references/

current-journal-article-resource-links-label = Ressourcenlinks

current-journal-article-resource-links-help = Der Prozentsatz der registrierten Inhalte, die Volltext-URLs in den Metadaten enthalten, um Forschern das einfache Auffinden von Inhalten für Text- und Data-Mining zu erleichtern. Erfahren Sie mehr: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

current-journal-article-ror-ids-label = RORs

current-journal-article-ror-ids-help = Der Prozentsatz des Inhalts, der Zugehörigkeitsmetadaten für Mitwirkende mit mindestens einer ROR-ID enthält, sodass Institutionen eindeutig identifiziert und ihre Forschungsergebnisse zurückverfolgt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

current-journal-article-similarity-checking-label = Ähnlichkeitsprüfung

current-journal-article-similarity-checking-help = Der Prozentsatz der registrierten Inhalte, die Volltextlinks für die Ähnlichkeitsprüfung enthalten, die es Mitgliedern ermöglichen, am Dienst teilzunehmen und Plagiate zu verhindern. Erfahren Sie mehr: https://www.crossref.org/documentation/similarity-check/participate/

current-journal-article-update-policies-label = Richtlinien aktualisieren

current-journal-article-update-policies-help = Der Prozentsatz der registrierten Inhalte, die den Crossmark-Dienst nutzen, der den Lesern einen schnellen und einfachen Zugriff auf den aktuellen Status eines Inhaltselements ermöglicht – unabhängig davon, ob dieser aktualisiert, korrigiert oder zurückgezogen wurde. Erfahren Sie mehr: https://www.crossref.org/services/crossmark/

backfile-journal-article-abstracts-label = Zusammenfassungen

backfile-journal-article-abstracts-help = Der Prozentsatz des Inhalts, der die Zusammenfassung in den Metadaten enthält, wodurch weitere Einblicke in den Inhalt des Werks gewährt und es besser auffindbar gemacht wird. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

backfile-journal-article-affiliations-label = Zugehörigkeiten

backfile-journal-article-affiliations-help = Der Prozentsatz des Inhalts, der Zugehörigkeitsmetadaten für Mitwirkende enthält, sodass die Forschungsergebnisse von Institutionen nachverfolgt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

backfile-journal-article-award-numbers-label = Auszeichnungen

backfile-journal-article-award-numbers-help = Der Prozentsatz der registrierten Inhalte, die mindestens eine Förderpreisnummer enthalten – eine von der Förderorganisation zugewiesene Nummer, um den spezifischen Förderbetrag (den Preis oder Zuschuss) zu identifizieren. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-descriptions-label = Beschreibungen

backfile-journal-article-descriptions-help = Der Prozentsatz der registrierten Inhalte, die eine Förder- oder Preisbeschreibung enthalten. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-funders-label = Geldgeber

backfile-journal-article-funders-help = Der Prozentsatz der registrierten Inhalte, die Metadaten über die Fördereinrichtung enthalten, die einen Zuschuss oder eine Auszeichnung gewährt. Durch die Erfassung von Fördermetadaten lässt sich der Zusammenhang zwischen Fördermitteln und Forschungsergebnissen nachvollziehen. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-licenses-label = Lizenzen

backfile-journal-article-licenses-help = Der Prozentsatz der registrierten Inhalte, die URLs enthalten, die auf eine Lizenz verweisen, die die Bedingungen erläutert, unter denen Leser auf Inhalte zugreifen können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/license/

backfile-journal-article-orcids-label = ORCID-IDs

backfile-journal-article-orcids-help = Der Prozentsatz des Inhalts, der ORCID-iDs enthält. Diese dauerhaften Identifikatoren ermöglichen es Benutzern, die Arbeit eines Forschers genau zu identifizieren – selbst wenn dieser Forscher einen Namen mit jemand anderem teilt oder wenn er seinen Namen ändert. Erfahren Sie mehr: https://www.crossref.org/documentation/reports/participation-reports/#00198

backfile-journal-article-references-label = Verweise

backfile-journal-article-references-help = Der Prozentsatz der registrierten Inhalte, die Referenzlisten in den Metadaten enthalten, sodass Zitate zwischen Forschungsergebnissen verfolgt und gezählt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/references/

backfile-journal-article-resource-links-label = Ressourcenlinks

backfile-journal-article-resource-links-help = Der Prozentsatz der registrierten Inhalte, die Volltext-URLs in den Metadaten enthalten, um Forschern das einfache Auffinden von Inhalten für Text- und Data-Mining zu erleichtern. Erfahren Sie mehr: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

backfile-journal-article-ror-ids-label = RORs

backfile-journal-article-ror-ids-help = Der Prozentsatz des Inhalts, der Zugehörigkeitsmetadaten für Mitwirkende mit mindestens einer ROR-ID enthält, sodass Institutionen eindeutig identifiziert und ihre Forschungsergebnisse zurückverfolgt werden können. Erfahren Sie mehr: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

backfile-journal-article-similarity-checking-label = Ähnlichkeitsprüfung

backfile-journal-article-similarity-checking-help = Der Prozentsatz der registrierten Inhalte, die Volltextlinks für die Ähnlichkeitsprüfung enthalten, die es Mitgliedern ermöglichen, am Dienst teilzunehmen und Plagiate zu verhindern. Erfahren Sie mehr: https://www.crossref.org/documentation/similarity-check/participate/

backfile-journal-article-update-policies-label = Richtlinien aktualisieren

backfile-journal-article-update-policies-help = Der Prozentsatz der registrierten Inhalte, die den Crossmark-Dienst nutzen, der den Lesern einen schnellen und einfachen Zugriff auf den aktuellen Status eines Inhaltselements ermöglicht – unabhängig davon, ob dieser aktualisiert, korrigiert oder zurückgezogen wurde. Erfahren Sie mehr: https://www.crossref.org/services/crossmark/

splash-screen =
  Mangostan
  
  ![Image](https://crossref.org/img/labs/creature2.svg)
  ### Warnungen, Vorbehalte und Wieselwörter
  
  Das ist ein [Crossref Labs](https://www.crossref.org/labs/) Version unserer Produktion [Particpation reports](https://www.crossref.org/members/prep/).
  
  Es kann zu Blutungen in den Augen kommen.
  
  
  ### Warum haben wir das getan?
  
  Wir lernen nie.
  
  ### Warum funktioniert „X“ nicht?
  
  Wir freuen uns, dass Sie gefragt haben. Der [source is available in GitLab](https://gitlab.com/crossref/labs/mangosteen) unter einer MIT Open Source-Lizenz. Wir haben es sogar mit Blick auf die Erweiterung gebaut. Es verwendet Standardbibliotheken wie [pandas](https://pandas.pydata.org/) und [Plotly](https://plotly.com/), um den Großteil seiner Arbeit zu erledigen. Und die gesamte Seite wird von den Besten betreut [Streamlit](https://streamlit.io/). Wir freuen uns auf Ihre Zusammenführungsanfragen!
  
  ### Können Sie die Widgets um 12 Pixel nach rechts verschieben?
  
  Wahrscheinlich nicht. Streamlit ist fantastisch, bietet Ihnen jedoch keine detaillierte Kontrolle über alle Elemente der Benutzeroberfläche.
  
  ### Wie schmeckt Mangostan?
  
  Überraschenderweise nicht schrecklich.
  
  ### Mag ich? Gefällt es Ihnen nicht? Mögen Sie Teile davon?
  
  Der Grund, warum wir diese Betaversion jetzt mit Ihnen teilen, ist, dass wir Feedback benötigen. Bitte!
  
  Sie können verwenden [Gitlab Issues](https://gitlab.com/crossref/labs/mangosteen/-/issues) Melden Sie Fehler, schreiben Sie Vorschläge und teilen Sie Ihre Gedanken. Danke schön!

