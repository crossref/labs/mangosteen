my-first-string = Anglais!

loading-data = Chargement des données...

splash-screen-heading = Écran de démarrage

results-screen-heading = Rapports

member-selector-heading = Sélecteur de membres

member-selector-label = Sélectionner un membre

member-selector-placeholder = Choisis une option

member-selector-help = Sélectionnez le(s) membre(s) Crossref pour lesquels vous souhaitez afficher les données de participation aux données

comment = Nouveau 

content-filter-heading = Filtre de contenu

period-filter-label = Période de temps

period-filter-help = Sélectionnez la période de publication pour laquelle vous souhaitez afficher les données de couverture. Les œuvres « actuelles » sont celles publiées au cours de l’année civile en cours ou des deux années civiles précédentes.

period-all-label = Tous

period-all-help = Contenu publié à tout moment, y compris le contenu actuel et antérieur

period-current-label = Actuel

period-current-help = Contenu publié au cours de l'année civile en cours ou des deux années civiles précédentes

period-backfile-label = Retour en arrière

period-back-file-help = Contenu publié il y a plus de deux années civiles

content-type-filter-label = Type de contenu

content-type-filter-help = Sélectionnez le type de contenu pour lequel vous souhaitez afficher les données de couverture

primary-name-label = Nom de membre

primary-name-help = Le nom principal de l’organisation membre

primary-resource-logo-label = Logo

primary-resource-logo-help = Le logo du membre extrait de Clearbit et basé sur le domaine de ressources principal du membre.

id-label = ID membres

id-help = L'identifiant unique du membre dans l'API REST Crossref (https://www.crossref.org/documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-api/)

account-type-label = Type de compte

non-profit-label = Non lucratif

date-joined-label = Date d'adhésion

annual-fee-label = Frais annuels

active-label = Actif

board-status-label = Statut du conseil d'administration

board-status-help = Le membre est-il un membre actuel ou passé du conseil d'administration de Crossref (https://www.crossref.org/board-and-governance/) ?

current-dois-label = Current DOIs

current-dois-help = Contenu publié au cours de l'année civile en cours ou des deux années civiles précédentes

backfile-dois-label = Rétroarchiver les DOI

backfile-dois-help = Contenu publié il y a plus de deux années civiles

total-dois-label = Nombre total de DOI

primary-resource-domain-label = Domaine de ressources principal

primary-resource-domain-help = Le domaine où est hébergé la plupart du contenu enregistré par ce membre

affiliations-current-label = Affiliations (actuelles)

similarity-checking-current-label = Vérification de similarité (actuel)

descriptions-current-label = Descriptions (actuelles)

ror-ids-current-label = ID ROR (actuels)

funders-backfile-label = Bailleurs de fonds (fichier rétrospectif)

licenses-backfile-label = Licences (backfile)

funders-current-label = Bailleurs de fonds (actuels)

affiliations-backfile-label = Affiliations (actuelles)

resource-links-backfile-label = Liens vers les ressources (backfile)

orcids-backfile-label = ORCID identifiants (backfile)

update-policies-current-label = Politiques de mise à jour (actuelles)

ror-ids-backfile-label = ID ROR (backfile)

orcids-current-label = ORCID identifiants (actuels)

similarity-checking-backfile-label = Vérification de similarité (backfile)

references-backfile-label = Références (backfile)

descriptions-backfile-label = Descriptions (fichier rétrospectif)

award-numbers-backfile-label = Numéros de récompense (fichier rétrospectif)

update-policies-backfile-label = Mettre à jour les politiques (backfile)

licenses-current-label = Licences (actuelles)

award-numbers-current-label = Numéros de récompense (actuels)

abstracts-backfile-label = Résumés (backfile)

resource-links-current-label = Liens vers les ressources (actuels)

abstracts-current-label = Résumés (actuels)

references-current-label = Références (actuelles)

overall-coverage-label = Couverture globale

overall-impact-label = Impact global

earliest-publication-year-label = Première année de publication

earliest-publication-year-help = La première année civile au cours de laquelle le membre a publié du contenu enregistré auprès de Crossref

latest-publication-year-label = Dernière année de publication

latest-publication-year-help = L'année civile la plus récente au cours de laquelle le membre a publié du contenu enregistré auprès de Crossref

2010-total-label =
  2010-total
  Étiquette-total-2011 = Total-2011
  Label-total-2012 = Total-2012
  Label-total-2013 = Total-2013
  Label-total-2014 = Total-2014
  Label-total-2015 = Total-2015
  Label-total-2016 = Total-2016
  Label-total-2017 = Total-2017
  Label-total-2018 = Total-2018
  Label-total-2019 = Total-2019
  Label-total-2020 = Total-2020
  Label-total-2021 = Total-2021

avg-pct-change-label = % de changement moyen PA

display-name-label = Afficher un nom

all-journal-article-abstracts-label = Résumés

all-journal-article-abstracts-help = Le pourcentage de contenu qui inclut le résumé dans les métadonnées, donnant un aperçu plus approfondi du contenu de l'œuvre et le rendant plus visible. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

all-journal-article-affiliations-label = Affiliations

all-journal-article-affiliations-help = Le pourcentage de contenu qui inclut des métadonnées d'affiliation pour les contributeurs, permettant de retracer les résultats de recherche des institutions. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

all-journal-article-award-numbers-label = Numéros de récompense

all-journal-article-award-numbers-help = Le pourcentage de contenu enregistré qui contient au moins un numéro de subvention de financement : un numéro attribué par l'organisme de financement pour identifier le financement spécifique (la récompense ou la subvention). L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-descriptions-label = Descriptions

all-journal-article-descriptions-help = Pourcentage de contenu enregistré contenant une description de subvention ou de récompense. L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-funders-label = Bailleurs de fonds

all-journal-article-funders-help = Pourcentage de contenu enregistré contenant des métadonnées sur l'organisme de financement accordant une subvention ou une récompense. L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-licenses-label = Licences

all-journal-article-licenses-help = Pourcentage de contenu enregistré contenant des URL pointant vers une licence expliquant les termes et conditions dans lesquels les lecteurs peuvent accéder au contenu. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/license/

all-journal-article-orcids-label = ORCID identifiants

all-journal-article-orcids-help = Le pourcentage de contenu contenant des identifiants ORCID. Ces identifiants persistants permettent aux utilisateurs d’identifier précisément le travail d’un chercheur, même lorsque ce chercheur partage un nom avec quelqu’un d’autre ou s’il change de nom. En savoir plus : https://www.crossref.org/documentation/reports/participation-reports/#00198

all-journal-article-references-label = Les références

all-journal-article-references-help = Le pourcentage de contenu enregistré qui inclut des listes de références dans les métadonnées, permettant de suivre et de compter les citations entre les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/references/

all-journal-article-resource-links-label = Liens vers les ressources

all-journal-article-resource-links-help = Pourcentage de contenu enregistré contenant des URL en texte intégral dans les métadonnées pour aider les chercheurs à localiser facilement le contenu pour l'exploration de texte et de données. En savoir plus : https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

all-journal-article-ror-ids-label = ROR

all-journal-article-ror-ids-help = Le pourcentage de contenu comprenant des métadonnées d'affiliation pour les contributeurs possédant au moins un identifiant ROR, permettant aux institutions d'être clairement identifiées et de retracer leurs résultats de recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

all-journal-article-similarity-checking-label = Vérification de similarité

all-journal-article-similarity-checking-help = Le pourcentage de contenu enregistré qui inclut des liens en texte intégral pour le contrôle de similarité, permettant aux membres de participer au service et d'éviter le plagiat. En savoir plus : https://www.crossref.org/documentation/similarity-check/participate/

all-journal-article-update-policies-label = Mettre à jour les politiques

all-journal-article-update-policies-help = Le pourcentage de contenu enregistré utilisant le service Crossmark, qui permet aux lecteurs d'accéder rapidement et facilement à l'état actuel d'un élément de contenu, qu'il ait été mis à jour, corrigé ou retiré. En savoir plus : https://www.crossref.org/services/crossmark/

current-journal-article-abstracts-help = Le pourcentage de contenu qui inclut le résumé dans les métadonnées, donnant un aperçu plus approfondi du contenu de l'œuvre et le rendant plus visible. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

current-journal-article-abstracts-label = Résumés

current-journal-article-affiliations-label = Affiliations

current-journal-article-affiliations-help = Le pourcentage de contenu qui inclut des métadonnées d'affiliation pour les contributeurs, permettant de retracer les résultats de recherche des institutions. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

current-journal-article-award-numbers-label = Prix

current-journal-article-award-numbers-help = Le pourcentage de contenu enregistré qui contient au moins un numéro de subvention de financement : un numéro attribué par l'organisme de financement pour identifier le financement spécifique (la récompense ou la subvention). L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-descriptions-label = Descriptions

current-journal-article-descriptions-help = Pourcentage de contenu enregistré contenant une description de subvention ou de récompense. L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-funders-label = Bailleurs de fonds

current-journal-article-funders-help = Pourcentage de contenu enregistré contenant des métadonnées sur l'organisme de financement accordant une subvention ou une récompense. L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-licenses-label = Licences

current-journal-article-licenses-help = Pourcentage de contenu enregistré contenant des URL pointant vers une licence expliquant les termes et conditions dans lesquels les lecteurs peuvent accéder au contenu. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/license/

current-journal-article-orcids-label = ORCID identifiants

current-journal-article-orcids-help = Le pourcentage de contenu contenant des identifiants ORCID. Ces identifiants persistants permettent aux utilisateurs d’identifier précisément le travail d’un chercheur, même lorsque ce chercheur partage un nom avec quelqu’un d’autre ou s’il change de nom. En savoir plus : https://www.crossref.org/documentation/reports/participation-reports/#00198

current-journal-article-references-label = Les références

current-journal-article-references-help = Le pourcentage de contenu enregistré qui inclut des listes de références dans les métadonnées, permettant de suivre et de compter les citations entre les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/references/

current-journal-article-resource-links-label = Liens vers les ressources

current-journal-article-resource-links-help = Pourcentage de contenu enregistré contenant des URL en texte intégral dans les métadonnées pour aider les chercheurs à localiser facilement le contenu pour l'exploration de texte et de données. En savoir plus : https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

current-journal-article-ror-ids-label = ROR

current-journal-article-ror-ids-help = Le pourcentage de contenu incluant des métadonnées d'affiliation pour les contributeurs possédant au moins un identifiant ROR, permettant aux institutions d'être clairement identifiées et de retracer leurs résultats de recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

current-journal-article-similarity-checking-label = Vérification de similarité

current-journal-article-similarity-checking-help = Le pourcentage de contenu enregistré qui inclut des liens en texte intégral pour le contrôle de similarité, permettant aux membres de participer au service et d'éviter le plagiat. En savoir plus : https://www.crossref.org/documentation/similarity-check/participate/

current-journal-article-update-policies-label = Mettre à jour les politiques

current-journal-article-update-policies-help = Le pourcentage de contenu enregistré utilisant le service Crossmark, qui permet aux lecteurs d'accéder rapidement et facilement à l'état actuel d'un élément de contenu, qu'il ait été mis à jour, corrigé ou retiré. En savoir plus : https://www.crossref.org/services/crossmark/

backfile-journal-article-abstracts-label = Résumés

backfile-journal-article-abstracts-help = Le pourcentage de contenu qui inclut le résumé dans les métadonnées, donnant un aperçu plus approfondi du contenu de l'œuvre et le rendant plus visible. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

backfile-journal-article-affiliations-label = Affiliations

backfile-journal-article-affiliations-help = Le pourcentage de contenu qui inclut des métadonnées d'affiliation pour les contributeurs, permettant de retracer les résultats de recherche des institutions. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

backfile-journal-article-award-numbers-label = Prix

backfile-journal-article-award-numbers-help = Le pourcentage de contenu enregistré qui contient au moins un numéro de subvention de financement : un numéro attribué par l'organisme de financement pour identifier le financement spécifique (la récompense ou la subvention). L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-descriptions-label = Descriptions

backfile-journal-article-descriptions-help = Pourcentage de contenu enregistré contenant une description de subvention ou de récompense. L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-funders-label = Bailleurs de fonds

backfile-journal-article-funders-help = Pourcentage de contenu enregistré contenant des métadonnées sur l'organisme de financement accordant une subvention ou une récompense. L’enregistrement des métadonnées de financement permet de retracer le lien entre le financement et les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-licenses-label = Licences

backfile-journal-article-licenses-help = Pourcentage de contenu enregistré contenant des URL pointant vers une licence expliquant les termes et conditions dans lesquels les lecteurs peuvent accéder au contenu. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/license/

backfile-journal-article-orcids-label = ORCID identifiants

backfile-journal-article-orcids-help = Le pourcentage de contenu contenant des identifiants ORCID. Ces identifiants persistants permettent aux utilisateurs d’identifier précisément le travail d’un chercheur, même lorsque ce chercheur partage un nom avec quelqu’un d’autre ou s’il change de nom. En savoir plus : https://www.crossref.org/documentation/reports/participation-reports/#00198

backfile-journal-article-references-label = Les références

backfile-journal-article-references-help = Le pourcentage de contenu enregistré qui inclut des listes de références dans les métadonnées, permettant de suivre et de compter les citations entre les résultats de la recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/references/

backfile-journal-article-resource-links-label = Liens vers les ressources

backfile-journal-article-resource-links-help = Pourcentage de contenu enregistré contenant des URL en texte intégral dans les métadonnées pour aider les chercheurs à localiser facilement le contenu pour l'exploration de texte et de données. En savoir plus : https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

backfile-journal-article-ror-ids-label = ROR

backfile-journal-article-ror-ids-help = Le pourcentage de contenu incluant des métadonnées d'affiliation pour les contributeurs possédant au moins un identifiant ROR, permettant aux institutions d'être clairement identifiées et de retracer leurs résultats de recherche. En savoir plus : https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

backfile-journal-article-similarity-checking-label = Vérification de similarité

backfile-journal-article-similarity-checking-help = Le pourcentage de contenu enregistré qui inclut des liens en texte intégral pour le contrôle de similarité, permettant aux membres de participer au service et d'éviter le plagiat. En savoir plus : https://www.crossref.org/documentation/similarity-check/participate/

backfile-journal-article-update-policies-label = Mettre à jour les politiques

backfile-journal-article-update-policies-help = Le pourcentage de contenu enregistré utilisant le service Crossmark, qui permet aux lecteurs d'accéder rapidement et facilement à l'état actuel d'un élément de contenu, qu'il ait été mis à jour, corrigé ou retiré. En savoir plus : https://www.crossref.org/services/crossmark/

splash-screen =
  Mangoustan
  
  ![Image](https://crossref.org/img/labs/creature2.svg)
  ### Avertissements, mises en garde et mots de fouine
  
  C'est un [Crossref Labs](https://www.crossref.org/labs/) version de notre production [Particpation reports](https://www.crossref.org/members/prep/).
  
  Cela pourrait faire saigner vos yeux.
  
  
  ### Pourquoi avons-nous fait cela ?
  
  Nous n'apprenons jamais.
  
  ### Pourquoi ne fait-il pas « X » ?
  
  Nous sommes heureux que vous ayez posé la question. Le [source is available in GitLab](https://gitlab.com/crossref/labs/mangosteen) sous licence Open Source MIT. Nous l’avons même construit dans un souci d’extension. Il utilise des bibliothèques standards comme [pandas](https://pandas.pydata.org/) et [Plotly](https://plotly.com/) pour faire l'essentiel de son travail. Et toute la page est pilotée par les plus excellents [Streamlit](https://streamlit.io/). Nous attendons avec impatience vos demandes de fusion !
  
  ### Pouvez-vous déplacer les widgets de 12 pixels vers la droite ?
  
  Probablement pas. Streamlit est fantastique, mais il ne vous donne pas un contrôle précis sur tous les éléments de l'interface utilisateur.
  
  ### Quel goût a le mangoustan ?
  
  Étonnamment, pas terrible.
  
  ### J'aime ça? Vous n'aimez pas ça ? Vous en aimez des morceaux ?
  
  La raison pour laquelle nous partageons cette version bêta avec vous maintenant est que nous avons besoin de commentaires. S'il te plaît!
  
  Vous pouvez utiliser [Gitlab Issues](https://gitlab.com/crossref/labs/mangosteen/-/issues) signalez les bugs, rédigez des suggestions et partagez vos réflexions. Merci!

