
my-first-string = English!
loading-data = Loading data...

 

splash-screen-heading = Splash Screen
results-screen-heading = Reports

member-name-label = Member name
member-selector-heading = Member selector
member-selector-label = Select member
member-selector-placeholder = Choose an option
member-selector-help = Select the Crossref member(s) for which you wish to display data participation data 

# The content filter allows the user to select subsections of content based
# on time period and content type.

content-filter-heading = Content filter

period-filter-label = Time period
period-filter-help = Select the publication time period for which you wish to display coverage data. “Current” works are those published in the current calendar year or the previous two calendar years.
period-all-label = All
period-all-help = Content published anytime, including current and backfile content
period-current-label = Current
period-current-help = Content published in the current calendar year or the previous two calendar years
period-backfile-label = Backfile
period-back-file-help = Content published more than two calendar years ago 

content-type-filter-label = Content type
content-type-filter-help = Select the content type for which you wish to display coverage data

# Column labels and help

primary-name-label = Member Name
primary-name-help = The member organization’s primary name
primary-resource-logo-label = Logo
primary-resource-logo-help = The member's logo as retrieved from Clearbit and based on the member's primary resource domain. 
id-label = Member ID
id-help = The member’s unique ID in the Crossref REST API (https://www.crossref.org/documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-api/)
account-type-label = Account Type
non-profit-label = Nonprofit
date-joined-label = Date Joined
annual-fee-label = Annual Fee
active-label = Active
board-status-label = Board Status
board-status-help = Is the member a current or past member of the Crossref board (https://www.crossref.org/board-and-governance/)?
current-dois-label = Current DOIs
current-dois-help =  Content published in the current calendar year or the two previous calendar years
backfile-dois-label = Backfile DOIs
backfile-dois-help = Content published more than two calendar years ago 
total-dois-label = Total DOIs
primary-resource-domain-label = Primary resource domain
primary-resource-domain-help = The domain where most of the content registered by this member is hosted
affiliations-current-label = Affiliations (current)
similarity-checking-current-label = Similarity Checking (current)
descriptions-current-label = Descriptions (current)
ror-ids-current-label = ROR IDs (current)
funders-backfile-label = Funders (backfile)
licenses-backfile-label = Licenses (backfile)
funders-current-label = Funders (current)
affiliations-backfile-label = Affiliations (current)
resource-links-backfile-label = Resource Links (backfile)
orcids-backfile-label = ORCID iDs (backfile)
update-policies-current-label = Update Policies (current)
ror-ids-backfile-label = ROR IDs (backfile)
orcids-current-label = ORCID iDs (current)
similarity-checking-backfile-label = Similarity Checking (backfile)
references-backfile-label = References (backfile)
descriptions-backfile-label = Descriptions (backfile)
award-numbers-backfile-label = Award Numbers (backfile)
update-policies-backfile-label = Update Policies (backfile)
licenses-current-label = Licenses (current)
award-numbers-current-label = Award Numbers (current)
abstracts-backfile-label = Abstracts (backfile)
resource-links-current-label = Resource Links (current)
abstracts-current-label = Abstracts (current)
references-current-label = References (current)
overall-coverage-label = Overall Coverage
overall-impact-label = Overall Impact
earliest-publication-year-label = Earliest publication year
earliest-publication-year-help = The earliest calendar year in which the member has published content registered with Crossref
latest-publication-year-label = Latest publication year
latest-publication-year-help = The most recent calendar year in which the member has published content registered with Crossref
2010-total-label = 2010-total
2011-total-label = 2011-total
2012-total-label = 2012-total
2013-total-label = 2013-total
2014-total-label = 2014-total
2015-total-label = 2015-total
2016-total-label = 2016-total
2017-total-label = 2017-total
2018-total-label = 2018-total
2019-total-label = 2019-total
2020-total-label = 2020-total
2021-total-label = 2021-total
avg-pct-change-label = Average % Change PA
display-name-label = Display Name
#breakdowns-dois-by-issued-year-label = DOIs registered per year
#breakdowns-dois-by-issued-year-help = How many DOIs has this member registered per year?
quarterlies-label = Quarterly files for download
quarterlies-help = Billing files amalgamated by quarter for download
account-type-help = The type of account the member has with Crossref
active-help = Is the member currently active?
non-profit-help = Is the member a nonprofit organization?
date-joined-help = The date the member joined Crossref
breakdowns-dois-by-issued-year-label = DOIs registered per year
breakdowns-dois-by-issued-year-help = How many DOIs has this member registered per year?
counts-type-label = Resource counts
counts-type-help = The number of resources of each type registered by the member
prefix-names-label = Prefix names
prefix-names-help = The names of the prefixes the member has registered with Crossref
resource-tlds-label = Resource TLDs
resource-tlds-help = The top-level domains of the resources the member has registered with Crossref
preservation-label = Preservation details
preservation-help = The member’s preservation details
resolutions-label = Resolution details
resolutions-help = The member’s resolution details
explore-latest-quarterly-deposit-report = Explore the latest quarterly deposit report
total-doi-resolutions = Total DOI resolutions
title-filter-label = Title filter

# New 


all-journal-article-abstracts-label = Abstracts
all-journal-article-abstracts-help = The percentage of content that includes the abstract in the metadata, giving further insights into the content of the work and making it more discoverable. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/
all-journal-article-affiliations-label = Affiliations
all-journal-article-affiliations-help = The percentage of content that includes affiliation metadata for contributors, allowing the research outputs of institutions to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/
all-journal-article-award-numbers-label = Award numbers
all-journal-article-award-numbers-help = The percentage of registered content that contains at least one funding award number - a number assigned by the funding organization to identify the specific piece of funding (the award or grant). Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
all-journal-article-descriptions-label = Descriptions
all-journal-article-descriptions-help = The percentage of registered content that contains a grant or award description. Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
all-journal-article-funders-label = Funders
all-journal-article-funders-help = The percentage of registered content that contains metadata about the funding body giving a grant or award. Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
all-journal-article-licenses-label = Licenses
all-journal-article-licenses-help = The percentage of registered content that contains URLs pointing to a license that explains the terms and conditions under which readers can access content. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/license/
all-journal-article-orcids-label = ORCID iDs
all-journal-article-orcids-help = The percentage of content containing ORCID iDs. These persistent identifiers enable users to precisely identify a researcher’s work - even when that researcher shares a name with someone else, or if they change their name. Learn more: https://www.crossref.org/documentation/reports/participation-reports/#00198
all-journal-article-references-label = References
all-journal-article-references-help = The percentage of registered content that includes reference lists in the metadata, allowing citations between research outputs to be tracked and counted. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/references/
all-journal-article-resource-links-label = Resource links
all-journal-article-resource-links-help = The percentage of registered content containing full-text URLs in the metadata to help researchers easily locate content for text and data mining. Learn more: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/
all-journal-article-ror-ids-label = RORs
all-journal-article-ror-ids-help = The percentage of content that includes affiliation metadata for contributors with at least one ROR ID, allowing institutions to be clearly identified and their research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/
all-journal-article-similarity-checking-label = Similarity checking
all-journal-article-similarity-checking-help = The percentage of registered content that includes full-text links for Similarity Check, allowing members to participate in the service and prevent plagiarism. Learn more: https://www.crossref.org/documentation/similarity-check/participate/
all-journal-article-update-policies-label = Update policies
all-journal-article-update-policies-help = The percentage of registered content using the Crossmark service, which gives readers quick and easy access to the current status of an item of content - whether it’s been updated, corrected, or retracted. Learn more: https://www.crossref.org/services/crossmark/


current-journal-article-abstracts-help = The percentage of content that includes the abstract in the metadata, giving further insights into the content of the work and making it more discoverable. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/
current-journal-article-abstracts-label = Abstracts
current-journal-article-affiliations-label = Affiliations
current-journal-article-affiliations-help = The percentage of content that includes affiliation metadata for contributors, allowing the research outputs of institutions to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/
current-journal-article-award-numbers-label = Awards
current-journal-article-award-numbers-help = The percentage of registered content that contains at least one funding award number - a number assigned by the funding organization to identify the specific piece of funding (the award or grant). Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
current-journal-article-descriptions-label = Descriptions
current-journal-article-descriptions-help = The percentage of registered content that contains a grant or award description. Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
current-journal-article-funders-label = Funders
current-journal-article-funders-help = The percentage of registered content that contains metadata about the funding body giving a grant or award. Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
current-journal-article-licenses-label = Licenses
current-journal-article-licenses-help = The percentage of registered content that contains URLs pointing to a license that explains the terms and conditions under which readers can access content. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/license/
current-journal-article-orcids-label = ORCID iDs
current-journal-article-orcids-help = The percentage of content containing ORCID iDs. These persistent identifiers enable users to precisely identify a researcher’s work - even when that researcher shares a name with someone else, or if they change their name. Learn more: https://www.crossref.org/documentation/reports/participation-reports/#00198
current-journal-article-references-label = References
current-journal-article-references-help = The percentage of registered content that includes reference lists in the metadata, allowing citations between research outputs to be tracked and counted. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/references/
current-journal-article-resource-links-label = Resource links
current-journal-article-resource-links-help = The percentage of registered content containing full-text URLs in the metadata to help researchers easily locate content for text and data mining. Learn more: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/
current-journal-article-ror-ids-label = RORs
current-journal-article-ror-ids-help = The percentage of content that includes affiliation metadata for contributors with at least one ROR ID, allowing institutions to be clearly identified and their research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/
current-journal-article-similarity-checking-label = Similarity Check
current-journal-article-similarity-checking-help = The percentage of registered content that includes full-text links for Similarity Check, allowing members to participate in the service and prevent plagiarism. Learn more: https://www.crossref.org/documentation/similarity-check/participate/
current-journal-article-update-policies-label = Update policies
current-journal-article-update-policies-help = The percentage of registered content using the Crossmark service, which gives readers quick and easy access to the current status of an item of content - whether it’s been updated, corrected, or retracted. Learn more: https://www.crossref.org/services/crossmark/


backfile-journal-article-abstracts-label = Abstracts
backfile-journal-article-abstracts-help = The percentage of content that includes the abstract in the metadata, giving further insights into the content of the work and making it more discoverable. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/
backfile-journal-article-affiliations-label = Affiliations
backfile-journal-article-affiliations-help = The percentage of content that includes affiliation metadata for contributors, allowing the research outputs of institutions to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/
backfile-journal-article-award-numbers-label = Awards
backfile-journal-article-award-numbers-help = The percentage of registered content that contains at least one funding award number - a number assigned by the funding organization to identify the specific piece of funding (the award or grant). Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
backfile-journal-article-descriptions-label = Descriptions
backfile-journal-article-descriptions-help = The percentage of registered content that contains a grant or award description. Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
backfile-journal-article-funders-label = Funders
backfile-journal-article-funders-help = The percentage of registered content that contains metadata about the funding body giving a grant or award. Registering funding metadata allows the link between funding and research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/funding/
backfile-journal-article-licenses-label = Licenses
backfile-journal-article-licenses-help = The percentage of registered content that contains URLs pointing to a license that explains the terms and conditions under which readers can access content. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/license/
backfile-journal-article-orcids-label = ORCID iDs
backfile-journal-article-orcids-help = The percentage of content containing ORCID iDs. These persistent identifiers enable users to precisely identify a researcher’s work - even when that researcher shares a name with someone else, or if they change their name. Learn more: https://www.crossref.org/documentation/reports/participation-reports/#00198
backfile-journal-article-references-label = References
backfile-journal-article-references-help = The percentage of registered content that includes reference lists in the metadata, allowing citations between research outputs to be tracked and counted. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/references/
backfile-journal-article-resource-links-label = Resource links
backfile-journal-article-resource-links-help = The percentage of registered content containing full-text URLs in the metadata to help researchers easily locate content for text and data mining. Learn more: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/
backfile-journal-article-ror-ids-label = RORs
backfile-journal-article-ror-ids-help = The percentage of content that includes affiliation metadata for contributors with at least one ROR ID, allowing institutions to be clearly identified and their research outputs to be traced. Learn more: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/
backfile-journal-article-similarity-checking-label = Similarity Checking
backfile-journal-article-similarity-checking-help = The percentage of registered content that includes full-text links for Similarity Check, allowing members to participate in the service and prevent plagiarism. Learn more: https://www.crossref.org/documentation/similarity-check/participate/
backfile-journal-article-update-policies-label = Update policies
backfile-journal-article-update-policies-help = The percentage of registered content using the Crossmark service, which gives readers quick and easy access to the current status of an item of content - whether it’s been updated, corrected, or retracted. Learn more: https://www.crossref.org/services/crossmark/

blank-domains-explainer =


    Note that a "blank" domain means that either:

    - The DOI was resolved form a non-HTTP source, like a PDF, ePub or local HTML file
    - The DOI link did not use HTTPS at some point in the resolution. See [Linking DOIs using HTTPs](https://www.crossref.org/blog/linking-dois-using-https-the-background-to-our-new-guidelines/)
    - The resolution came from a party that runs a privacy proxy which strips relevant headers from the request.

    And a DOI domain (e.g. `doi.org` or `dx.doi.org`) likely indicates:
     - The DOI was access via the `doi.org` website (often via a bot, so don't get too excited)
     - a DOI alias or a redirect. It can also (rarely) be a circular reference (a DOI pointing to itself).


splash-screen =

    Mangosteen

    ![Image](https://crossref.org/img/labs/creature2.svg)
    ### Warnings, Caveats and Weasel Words

    This is a [Crossref Labs](https://www.crossref.org/labs/) version of our production [Particpation reports](https://www.crossref.org/members/prep/).

    It may cause your eyes to bleed.


    ### Why have we done this?

    We never learn.

    ### Why doesn't it do `X`?

    We're glad you asked. The [source is available in GitLab](https://gitlab.com/crossref/labs/mangosteen) under an MIT Open Source License. We even built it with an eye toward extension. It uses standard libraries like [pandas](https://pandas.pydata.org/) and [Plotly](https://plotly.com/) to do most of its work. And the enitre page is driven by the most excellent [Streamlit](https://streamlit.io/). We look forward to your merge requests!

    ### Can you move the widgets 12 pixels to the right?

    Probably not. Streamlit is fantastic, but it doesn't give you fine-grained control over all UI elements.

    ### What does mangosteen taste like?

    We've only tried the juice. It is very, very sweet. Just like we are.
    
    ### Like it? Don’t like it? Like bits of it? 
    
    The reason we’re sharing this beta version with you now is that we need feedback. Please!

    You can use [Gitlab Issues](https://gitlab.com/crossref/labs/mangosteen/-/issues) report bugs, write up suggestions, and share your thoughts. Thank you!



