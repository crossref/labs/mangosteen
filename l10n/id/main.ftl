my-first-string = Bahasa inggris!

loading-data = Memuat data...

splash-screen-heading = Layar Pembuka

results-screen-heading = Laporan

member-selector-heading = Pemilih anggota

member-selector-label = Pilih anggota

member-selector-placeholder = Pilih satu opsi

member-selector-help = Pilih anggota Crossref yang data partisipasi datanya ingin Anda tampilkan

comment = Baru 

content-filter-heading = Penyaring konten

period-filter-label = Jangka waktu

period-filter-help = Pilih periode waktu publikasi yang ingin Anda tampilkan data cakupannya. Karya “saat ini” adalah karya yang diterbitkan pada tahun kalender berjalan atau dua tahun kalender sebelumnya.

period-all-label = Semua

period-all-help = Konten diterbitkan kapan saja, termasuk konten terkini dan konten cadangan

period-current-label = Saat ini

period-current-help = Konten yang dipublikasikan pada tahun kalender saat ini atau dua tahun kalender sebelumnya

period-backfile-label = File belakang

period-back-file-help = Konten diterbitkan lebih dari dua tahun kalender yang lalu

content-type-filter-label = Jenis konten

content-type-filter-help = Pilih tipe konten yang ingin Anda tampilkan data cakupannya

primary-name-label = Nama anggota

primary-name-help = Nama utama organisasi anggota

primary-resource-logo-label = Logo

primary-resource-logo-help = Logo anggota diambil dari Clearbit dan berdasarkan domain sumber daya utama anggota.

id-label = Tanda Anggota

id-help = ID unik anggota di Crossref REST API (https://www.crossref.org/documentation/retrieve-metadata/rest-api/a-non-technical-introduction-to-our-api/)

account-type-label = Jenis akun

non-profit-label = Nirlaba

date-joined-label = Tanggal Bergabung

annual-fee-label = Biaya tahunan

active-label = Aktif

board-status-label = Status Dewan

board-status-help = Apakah anggota tersebut merupakan anggota dewan Crossref saat ini atau sebelumnya (https://www.crossref.org/board-and-governance/)?

current-dois-label = DOI saat ini

current-dois-help = Konten yang dipublikasikan pada tahun kalender saat ini atau dua tahun kalender sebelumnya

backfile-dois-label = DOI file belakang

backfile-dois-help = Konten diterbitkan lebih dari dua tahun kalender yang lalu

total-dois-label = Jumlah DOI

primary-resource-domain-label = Domain sumber daya utama

primary-resource-domain-help = Domain tempat sebagian besar konten yang didaftarkan oleh anggota ini dihosting

affiliations-current-label = Afiliasi (saat ini)

similarity-checking-current-label = Pemeriksaan Kesamaan (saat ini)

descriptions-current-label = Deskripsi (saat ini)

ror-ids-current-label = ID ROR (saat ini)

funders-backfile-label = Pemberi dana (file belakang)

licenses-backfile-label = Lisensi (file belakang)

funders-current-label = Pemberi dana (saat ini)

affiliations-backfile-label = Afiliasi (saat ini)

resource-links-backfile-label = Tautan Sumber Daya (file belakang)

orcids-backfile-label = iD ORCID (file belakang)

update-policies-current-label = Perbarui Kebijakan (saat ini)

ror-ids-backfile-label = ID ROR (file belakang)

orcids-current-label = iD ORCID (saat ini)

similarity-checking-backfile-label = Pemeriksaan Kesamaan (backfile)

references-backfile-label = Referensi (file belakang)

descriptions-backfile-label = Deskripsi (file belakang)

award-numbers-backfile-label = Nomor Penghargaan (file belakang)

update-policies-backfile-label = Kebijakan Pembaruan (file belakang)

licenses-current-label = Lisensi (saat ini)

award-numbers-current-label = Nomor Penghargaan (saat ini)

abstracts-backfile-label = Abstrak (file belakang)

resource-links-current-label = Tautan Sumber Daya (saat ini)

abstracts-current-label = Abstrak (saat ini)

references-current-label = Referensi (saat ini)

overall-coverage-label = Cakupan Keseluruhan

overall-impact-label = Dampak Keseluruhan

earliest-publication-year-label = Tahun penerbitan paling awal

earliest-publication-year-help = Tahun kalender paling awal saat anggota menerbitkan konten yang terdaftar di Crossref

latest-publication-year-label = Tahun penerbitan terakhir

latest-publication-year-help = Tahun kalender terbaru saat anggota menerbitkan konten yang terdaftar di Crossref

2010-total-label =
  2010-total
  Label total 2011 = total 2011
  Label total 2012 = total 2012
  Label total 2013 = total 2013
  Label total 2014 = total 2014
  Label total 2015 = total 2015
  Label total 2016 = total 2016
  Label total 2017 = total 2017
  Label total 2018 = total 2018
  Label total 2019 = total 2019
  Label total 2020 = total 2020
  Label total 2021 = total 2021

avg-pct-change-label = Rata-rata % Perubahan PA

display-name-label = Nama tampilan

all-journal-article-abstracts-label = Abstrak

all-journal-article-abstracts-help = Persentase konten yang menyertakan abstrak dalam metadata, memberikan wawasan lebih lanjut tentang konten karya dan membuatnya lebih mudah ditemukan. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

all-journal-article-affiliations-label = Afiliasi

all-journal-article-affiliations-help = Persentase konten yang menyertakan metadata afiliasi untuk kontributor, sehingga keluaran penelitian dari institusi dapat ditelusuri. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

all-journal-article-award-numbers-label = Nomor penghargaan

all-journal-article-award-numbers-help = Persentase konten terdaftar yang berisi setidaknya satu nomor penghargaan pendanaan - nomor yang ditetapkan oleh organisasi pemberi dana untuk mengidentifikasi bagian pendanaan tertentu (penghargaan atau hibah). Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-descriptions-label = Deskripsi

all-journal-article-descriptions-help = Persentase konten terdaftar yang berisi deskripsi hibah atau penghargaan. Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-funders-label = Pemberi dana

all-journal-article-funders-help = Persentase konten terdaftar yang berisi metadata tentang lembaga pendanaan yang memberikan hibah atau penghargaan. Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

all-journal-article-licenses-label = Lisensi

all-journal-article-licenses-help = Persentase konten terdaftar yang berisi URL yang mengarah ke lisensi yang menjelaskan syarat dan ketentuan di mana pembaca dapat mengakses konten. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/license/

all-journal-article-orcids-label = iD ORCID

all-journal-article-orcids-help = Persentase konten yang mengandung iD ORCID. Pengidentifikasi persisten ini memungkinkan pengguna mengidentifikasi karya peneliti dengan tepat - bahkan ketika peneliti tersebut berbagi nama dengan orang lain, atau jika mereka mengubah namanya. Pelajari lebih lanjut: https://www.crossref.org/documentation/reports/participation-reports/#00198

all-journal-article-references-label = Referensi

all-journal-article-references-help = Persentase konten terdaftar yang menyertakan daftar referensi dalam metadata, sehingga kutipan di antara keluaran penelitian dapat dilacak dan dihitung. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/references/

all-journal-article-resource-links-label = Tautan sumber daya

all-journal-article-resource-links-help = Persentase konten terdaftar yang berisi URL teks lengkap dalam metadata untuk membantu peneliti dengan mudah menemukan konten untuk teks dan penambangan data. Pelajari lebih lanjut: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

all-journal-article-ror-ids-label = ROR

all-journal-article-ror-ids-help = Persentase konten yang menyertakan metadata afiliasi untuk kontributor yang memiliki setidaknya satu ID ROR, sehingga institusi dapat diidentifikasi dengan jelas dan hasil penelitiannya dapat ditelusuri. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

all-journal-article-similarity-checking-label = Pemeriksaan kesamaan

all-journal-article-similarity-checking-help = Persentase konten terdaftar yang menyertakan tautan teks lengkap untuk Pemeriksaan Kesamaan, memungkinkan anggota untuk berpartisipasi dalam layanan dan mencegah plagiarisme. Pelajari lebih lanjut: https://www.crossref.org/documentation/similarity-check/participate/

all-journal-article-update-policies-label = Perbarui kebijakan

all-journal-article-update-policies-help = Persentase konten yang didaftarkan menggunakan layanan Crossmark, yang memberi pembaca akses cepat dan mudah ke status terkini suatu item konten - apakah sudah diperbarui, diperbaiki, atau dicabut. Pelajari lebih lanjut: https://www.crossref.org/services/crossmark/

current-journal-article-abstracts-help = Persentase konten yang menyertakan abstrak dalam metadata, memberikan wawasan lebih lanjut tentang konten karya dan membuatnya lebih mudah ditemukan. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

current-journal-article-abstracts-label = Abstrak

current-journal-article-affiliations-label = Afiliasi

current-journal-article-affiliations-help = Persentase konten yang menyertakan metadata afiliasi untuk kontributor, sehingga keluaran penelitian dari institusi dapat ditelusuri. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

current-journal-article-award-numbers-label = Penghargaan

current-journal-article-award-numbers-help = Persentase konten terdaftar yang berisi setidaknya satu nomor penghargaan pendanaan - nomor yang ditetapkan oleh organisasi pemberi dana untuk mengidentifikasi bagian pendanaan tertentu (penghargaan atau hibah). Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-descriptions-label = Deskripsi

current-journal-article-descriptions-help = Persentase konten terdaftar yang berisi deskripsi hibah atau penghargaan. Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-funders-label = Pemberi dana

current-journal-article-funders-help = Persentase konten terdaftar yang berisi metadata tentang lembaga pendanaan yang memberikan hibah atau penghargaan. Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

current-journal-article-licenses-label = Lisensi

current-journal-article-licenses-help = Persentase konten terdaftar yang berisi URL yang mengarah ke lisensi yang menjelaskan syarat dan ketentuan di mana pembaca dapat mengakses konten. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/license/

current-journal-article-orcids-label = iD ORCID

current-journal-article-orcids-help = Persentase konten yang mengandung iD ORCID. Pengidentifikasi persisten ini memungkinkan pengguna mengidentifikasi karya peneliti secara tepat - bahkan ketika peneliti tersebut berbagi nama dengan orang lain, atau jika mereka mengubah namanya. Pelajari lebih lanjut: https://www.crossref.org/documentation/reports/participation-reports/#00198

current-journal-article-references-label = Referensi

current-journal-article-references-help = Persentase konten terdaftar yang menyertakan daftar referensi dalam metadata, sehingga kutipan di antara keluaran penelitian dapat dilacak dan dihitung. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/references/

current-journal-article-resource-links-label = Tautan sumber daya

current-journal-article-resource-links-help = Persentase konten terdaftar yang berisi URL teks lengkap dalam metadata untuk membantu peneliti dengan mudah menemukan konten untuk teks dan penambangan data. Pelajari lebih lanjut: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

current-journal-article-ror-ids-label = ROR

current-journal-article-ror-ids-help = Persentase konten yang menyertakan metadata afiliasi untuk kontributor yang memiliki setidaknya satu ID ROR, sehingga institusi dapat diidentifikasi dengan jelas dan hasil penelitiannya dapat ditelusuri. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

current-journal-article-similarity-checking-label = Pemeriksaan Kemiripan

current-journal-article-similarity-checking-help = Persentase konten terdaftar yang menyertakan tautan teks lengkap untuk Pemeriksaan Kesamaan, memungkinkan anggota untuk berpartisipasi dalam layanan dan mencegah plagiarisme. Pelajari lebih lanjut: https://www.crossref.org/documentation/similarity-check/participate/

current-journal-article-update-policies-label = Perbarui kebijakan

current-journal-article-update-policies-help = Persentase konten yang didaftarkan menggunakan layanan Crossmark, yang memberi pembaca akses cepat dan mudah ke status terkini suatu item konten - apakah sudah diperbarui, diperbaiki, atau dicabut. Pelajari lebih lanjut: https://www.crossref.org/services/crossmark/

backfile-journal-article-abstracts-label = Abstrak

backfile-journal-article-abstracts-help = Persentase konten yang menyertakan abstrak dalam metadata, memberikan wawasan lebih lanjut tentang konten karya dan membuatnya lebih mudah ditemukan. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/abstracts/

backfile-journal-article-affiliations-label = Afiliasi

backfile-journal-article-affiliations-help = Persentase konten yang menyertakan metadata afiliasi untuk kontributor, sehingga keluaran penelitian dari institusi dapat ditelusuri. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

backfile-journal-article-award-numbers-label = Penghargaan

backfile-journal-article-award-numbers-help = Persentase konten terdaftar yang berisi setidaknya satu nomor penghargaan pendanaan - nomor yang ditetapkan oleh organisasi pemberi dana untuk mengidentifikasi bagian pendanaan tertentu (penghargaan atau hibah). Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-descriptions-label = Deskripsi

backfile-journal-article-descriptions-help = Persentase konten terdaftar yang berisi deskripsi hibah atau penghargaan. Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-funders-label = Pemberi dana

backfile-journal-article-funders-help = Persentase konten terdaftar yang berisi metadata tentang lembaga pendanaan yang memberikan hibah atau penghargaan. Mendaftarkan metadata pendanaan memungkinkan penelusuran hubungan antara pendanaan dan keluaran penelitian. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/funding/

backfile-journal-article-licenses-label = Lisensi

backfile-journal-article-licenses-help = Persentase konten terdaftar yang berisi URL yang mengarah ke lisensi yang menjelaskan syarat dan ketentuan di mana pembaca dapat mengakses konten. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/license/

backfile-journal-article-orcids-label = iD ORCID

backfile-journal-article-orcids-help = Persentase konten yang mengandung iD ORCID. Pengidentifikasi persisten ini memungkinkan pengguna mengidentifikasi karya peneliti dengan tepat - bahkan ketika peneliti tersebut berbagi nama dengan orang lain, atau jika mereka mengubah namanya. Pelajari lebih lanjut: https://www.crossref.org/documentation/reports/participation-reports/#00198

backfile-journal-article-references-label = Referensi

backfile-journal-article-references-help = Persentase konten terdaftar yang menyertakan daftar referensi dalam metadata, sehingga kutipan di antara keluaran penelitian dapat dilacak dan dihitung. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/references/

backfile-journal-article-resource-links-label = Tautan sumber daya

backfile-journal-article-resource-links-help = Persentase konten terdaftar yang berisi URL teks lengkap dalam metadata untuk membantu peneliti dengan mudah menemukan konten untuk teks dan penambangan data. Pelajari lebih lanjut: https://www.crossref.org/documentation/retrieve-metadata/rest-api/text-and-data-mining/

backfile-journal-article-ror-ids-label = ROR

backfile-journal-article-ror-ids-help = Persentase konten yang menyertakan metadata afiliasi untuk kontributor yang memiliki setidaknya satu ID ROR, sehingga institusi dapat diidentifikasi dengan jelas dan hasil penelitiannya dapat ditelusuri. Pelajari lebih lanjut: https://www.crossref.org/documentation/principles-practices/best-practices/bibliographic/

backfile-journal-article-similarity-checking-label = Pemeriksaan Kesamaan

backfile-journal-article-similarity-checking-help = Persentase konten terdaftar yang menyertakan tautan teks lengkap untuk Pemeriksaan Kesamaan, memungkinkan anggota untuk berpartisipasi dalam layanan dan mencegah plagiarisme. Pelajari lebih lanjut: https://www.crossref.org/documentation/similarity-check/participate/

backfile-journal-article-update-policies-label = Perbarui kebijakan

backfile-journal-article-update-policies-help = Persentase konten yang didaftarkan menggunakan layanan Crossmark, yang memberi pembaca akses cepat dan mudah ke status terkini suatu item konten - apakah sudah diperbarui, diperbaiki, atau dicabut. Pelajari lebih lanjut: https://www.crossref.org/services/crossmark/

splash-screen =
  Manggis
  
  ![Image](https://crossref.org/img/labs/creature2.svg)
  ### Peringatan, Peringatan dan Kata Musang
  
  Ini adalah sebuah [Crossref Labs](https://www.crossref.org/labs/) versi produksi kami [Particpation reports](https://www.crossref.org/members/prep/).
  
  Hal ini dapat menyebabkan mata Anda berdarah.
  
  
  ### Mengapa kami melakukan ini?
  
  Kami tidak pernah belajar.
  
  ### Mengapa tidak melakukan `X`?
  
  Kami senang Anda bertanya. Itu [source is available in GitLab](https://gitlab.com/crossref/labs/mangosteen) di bawah Lisensi Sumber Terbuka MIT. Kami bahkan membangunnya dengan tujuan perluasan. Ini menggunakan perpustakaan standar seperti [pandas](https://pandas.pydata.org/) dan [Plotly](https://plotly.com/) untuk melakukan sebagian besar pekerjaannya. Dan seluruh halaman didorong oleh yang paling bagus [Streamlit](https://streamlit.io/). Kami menantikan permintaan penggabungan Anda!
  
  ### Bisakah Anda memindahkan widget 12 piksel ke kanan?
  
  Mungkin tidak. Streamlit memang luar biasa, tetapi tidak memberi Anda kendali penuh atas semua elemen UI.
  
  ### Seperti apa rasanya manggis?
  
  Anehnya tidak buruk.
  
  ### Suka itu? Tidak menyukainya? Suka sebagian?
  
  Alasan kami membagikan versi beta ini kepada Anda sekarang adalah karena kami memerlukan masukan. Silakan!
  
  Anda dapat gunakan [Gitlab Issues](https://gitlab.com/crossref/labs/mangosteen/-/issues) laporkan bug, tulis saran, dan bagikan pemikiran Anda. Terima kasih!

