from streamlit.testing.v1 import AppTest
from mangosteen.settings import DEFAULT_STATE, DEFAULT_SHARED_STATE


def test_smoke():
    at = AppTest("app.py", default_timeout=60).run()
    assert not at.exception


def test_default_session_state():
    at = AppTest("app.py", default_timeout=60).run()
    assert all(at.session_state[k] == v for k, v in DEFAULT_STATE.items())
    assert all(at.session_state[k] == v for k, v in DEFAULT_SHARED_STATE.items())


def test_language_param():
    at = AppTest("app.py", default_timeout=60)
    at.query_params = {"language": "French"}
    at.run()
    assert not at.exception
    assert at.session_state["language"] == "French"
    assert at.session_state["_language"] == "French"


def test_selected_language():
    at = AppTest("app.py", default_timeout=60)
    at.run()
    assert not at.exception
    assert at.sidebar.selectbox(key="_language").value == "English"
    at.sidebar.selectbox(key="_language").select("German").run()
    assert at.session_state["_language"] == "German"
    assert at.session_state["language"] == "German"


def test_selected_members_params():
    at = AppTest("app.py", default_timeout=60)
    at.query_params = {"selected-member-ids": [4374, 340]}
    at.run()
    assert not at.exception
    assert at.session_state["selected-member-names"] == [
        "eLife Sciences Publications, Ltd",
        "Public Library of Science (PLoS)",
    ]


def test_remove_a_selected_member():
    at = AppTest("app.py", default_timeout=60)
    at.run()
    assert not at.exception
    assert at.sidebar.multiselect(key="selected-member-names").value == []
    at.sidebar.multiselect(key="selected-member-names").select(
        "eLife Sciences Publications, Ltd"
    ).select("Public Library of Science (PLoS)").unselect(
        "Public Library of Science (PLoS)"
    ).run()
    assert at.session_state["selected-member-names"] == [
        "eLife Sciences Publications, Ltd",
    ]
