# from mangosteen.gridify import create_grid_spec


# def test_create_grid_spec():
#     # Test with default label_width and column_width
#     result = create_grid_spec(3, 2)
#     expected = [[1, 2, 2], [1, 2, 2], [1, 2, 2]]
#     assert result == expected, f"Expected {expected}, but got {result}"


# def test_create_grid_spec_with_custom_label_width():
#     # Test with custom label_width
#     result = create_grid_spec(3, 2, label_width=5)
#     expected = [[5, 2, 2], [5, 2, 2], [5, 2, 2]]
#     assert result == expected, f"Expected {expected}, but got {result}"


# def test_create_grid_spec_with_custom_column_width():
#     # Test with custom column_width
#     result = create_grid_spec(3, 2, column_width=2)
#     expected = [[10, 2, 2], [10, 2, 2], [10, 2, 2]]
#     assert result == expected, f"Expected {expected}, but got {result}"


# def test_create_grid_spec_with_custom_label_and_column_width():
#     # Test with custom label_width and column_width
#     result = create_grid_spec(3, 2, label_width=5, column_width=2)
#     expected = [[5, 2, 2], [5, 2, 2], [5, 2, 2]]
#     assert result == expected, f"Expected {expected}, but got {result}"


import pytest
from mangosteen.ui_data_point import create_grid_spec

# Constants used in the function, assuming they are defined elsewhere
# LABEL_WIDTH = 1
# COLUMN_WIDTH = 2


# Happy path tests with various realistic test values
@pytest.mark.parametrize(
    "number_of_columns, number_of_rows, expected",
    [
        (1, 1, [[1, 2]]),
        (2, 3, [[1, 2, 2, 2], [1, 2, 2, 2]]),
        (
            5,
            10,
            [
                [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            ],
        ),
    ],
    ids=["single-cell-grid", "multi-cell-grid-small", "multi-cell-grid-big"],
)
def test_create_grid_spec_happy_path(number_of_columns, number_of_rows, expected):
    # Act
    result = create_grid_spec(number_of_columns, number_of_rows)

    # Assert
    assert result == expected, f"Failed for test ID: {pytest.current_test.id}"


# Edge cases
@pytest.mark.parametrize(
    "number_of_columns, number_of_rows, expected",
    [
        (0, 0, []),
        (1, 0, [[1]]),
        (0, 1, []),
    ],
    ids=["zero-columns-rows", "zero-rows", "zero-columns"],
)
def test_create_grid_spec_edge_cases(number_of_columns, number_of_rows, expected):
    # Act
    result = create_grid_spec(number_of_columns, number_of_rows)

    # Assert
    assert result == expected, f"Failed for test ID: {pytest.current_test.id}"


# Error cases
@pytest.mark.parametrize(
    "number_of_columns, number_of_rows",
    [
        (0.5, 1),
        (1, 0.5),
        ("two", 1),
        (1, "one"),
    ],
    ids=["float-columns", "float-rows", "string-columns", "string-rows"],
)
def test_create_grid_spec_error_cases(number_of_columns, number_of_rows):
    # Act and Assert
    with pytest.raises(TypeError):
        create_grid_spec(number_of_columns, number_of_rows)
