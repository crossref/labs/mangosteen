import logging
from time import time

# from mangosteen.ui_download_examples import get_summary_data, display_element
from urllib.parse import parse_qs, urlencode, urlparse

import pandas as pd
import requests
import streamlit as st
from slugify import slugify

from mangosteen.l10n import _
from mangosteen.sessions import get_global_session_state, set_global_session_state
from mangosteen.settings import (
    CODE_NAME,
    API_URI,
    HEADERS,
    REPORTS_PAGE,
    REPORTS_PAGE_ICON,
    SERVICE_NAME,
)
from mangosteen.data import lookup_filter, rest_api_period_filter

from mangosteen.ui_common import insert_css

ROWS_PER_REQUEST = 500
MAX_DOWNLOAD_SIZE = 5000

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def download_file_name(summary_data) -> str:
    # Construct the base filename from the summary data
    base_filename = f"{summary_data['download-example-member-name-label']}-{summary_data['download-example-period-label']}-missing-{summary_data['download-example-missing-metadata-label']}"

    # Slugify the base filename and append the .csv extension
    safe_filename = slugify(base_filename) + ".csv"

    return safe_filename


def get_download_example_column_config():
    return {
        "metadata-link": st.column_config.LinkColumn(
            "Metadata Link",
            help="A link to the metadata for this DOI",
        ),
        "doi": st.column_config.LinkColumn(
            "DOI",
            width=25,
            help="DOI of record missing metadata",
        ),
        "title": st.column_config.TextColumn(
            "Title", help="Title of record missing metadata"
        ),
    }


def display_element(label: str, text: str):
    st.write(
        f"<span class='cr-gridify-row-label'>{_(label)}</span>: {text}",
        unsafe_allow_html=True,
    )


def get_population(population_url: str):
    t1 = time()
    total = requests.get(population_url).json()["message"]["total-results"]
    return time() - t1, int(total)


def convert_to_crossref_style_filter(filters):
    value = ",".join([f"{k}:{v}" for k, v in filters.items()])
    return {"filter": value}


def build_base_query_url_from_streamlit() -> str:

    member_id = get_global_session_state("download-example")["member-id"]
    path = f"members/{member_id}/works"

    content_type = get_global_session_state("download-example")["content-type"]
    content_type_filter = {"type": content_type}

    selected_period = get_global_session_state("selected-period")
    period_filter = rest_api_period_filter(selected_period)

    missing_metadata_filter_key = get_global_session_state("download-example")[
        "filter-key"
    ]
    missing_metadata_filter = lookup_filter(missing_metadata_filter_key)

    combined_filters = missing_metadata_filter | period_filter | content_type_filter
    crossref_style_filter_parameter = convert_to_crossref_style_filter(combined_filters)

    prepared_url = requests.Request(
        method="GET",
        url=f"{API_URI}/{path}",
        params=crossref_style_filter_parameter,
    ).prepare()
    return prepared_url.url


def get_summary_data():
    member_name = get_global_session_state("download-example")["member-name"]
    data_label = get_global_session_state("download-example")["data-label"]
    content_type = get_global_session_state("download-example")["content-type"]
    selected_period = get_global_session_state("selected-period")

    missing_metadata_test_url = build_base_query_url_from_streamlit()
    missing_metadata_request_time, missing_metadata_count = get_population(
        missing_metadata_test_url
    )
    sample_needed = missing_metadata_count > MAX_DOWNLOAD_SIZE
    if sample_needed:
        missing_metadata_test_url + f"&sample=100"

    return {
        "download-example-member-name-label": member_name,
        "download-example-content-type-label": content_type,
        "download-example-missing-metadata-label": data_label,
        "download-example-period-label": selected_period,
        "missing-metadata-count-label": missing_metadata_count,
        "missing-metadata-api-url-label": missing_metadata_test_url,
        "_request-time": missing_metadata_request_time,
        "_sample-needed": sample_needed,
    }


def has_param(url: str, param: str) -> bool:
    return param in parse_qs(urlparse(url).query)


def is_sample_url(url: str) -> bool:
    return has_param(url, "sample")


def all_results_offset(
    url, rows=ROWS_PER_REQUEST, headers=None, status_container=None
) -> list:

    set_global_session_state("download-status", "downloading")
    if not status_container:
        status_container = st

    with status_container:
        with st.status(
            _("retrieving-example-metadata-message"), expanded=True, state="running"
        ) as status:
            st.write(_("using-rest-api-message"))

            params = {} if is_sample_url(url) else {"offset": 0, "rows": rows}
            all_items = []
            with requests.Session() as s:
                while True:

                    prepared = requests.Request(
                        method="GET", url=url, headers=headers, params=params
                    ).prepare()
                    logger.info(f">>>> all_results_offset: prepared={prepared.url}")

                    res = s.send(prepared)
                    if res.status_code != 200:
                        raise ConnectionError(f"The REST API returned code {res.code}")

                    results = res.json()
                    expected_number_of_items = min(
                        results["message"]["total-results"], MAX_DOWNLOAD_SIZE
                    )
                    all_items += results["message"]["items"]
                    st.write(
                        f"{_('records-retrieved-label')} : {len(all_items):,} / {expected_number_of_items:,}"
                    )

                    if (
                        len(results["message"]["items"]) == 0
                        or len(all_items) >= MAX_DOWNLOAD_SIZE
                    ):
                        st.write(
                            f"{_('final-retrieval-message')} {len(all_items):,} / {expected_number_of_items:,}"
                        )
                        status.update(
                            label=_("finished-retrieval-message"),
                            state="complete",
                            expanded=False,
                        )
                        logger.info(
                            f">>>> all_results_offset: Finished: retrieved {len(all_items)} items"
                        )
                        set_global_session_state("download-results", all_items)
                        set_global_session_state("download-status", "complete")
                        return all_items

                    if is_sample_url(url):
                        continue

                    # more to go?
                    params["offset"] += rows


def display_reports_page_button() -> None:
    if st.button(_("reports-page-button-label"), help=_("reports-page-button-help")):
        set_global_session_state("download-status", "idle")
        set_global_session_state("download-example", False)
        st.switch_page("app.py")


def metadata_link(doi: str) -> str:
    return f"https://api.crossref.org/works/{doi}.xml"


def summarize_results(results: list) -> pd.DataFrame:
    doi_title_list = [
        (item["DOI"], item["title"][0], metadata_link(item["DOI"])) for item in results
    ]
    return pd.DataFrame(
        doi_title_list, columns=["doi", "title", "metadata-link"]
    )  # .to_string(index=False)


def display_summary_container(summary_data: dict) -> None:
    sample_needed = summary_data["missing-metadata-count-label"] > MAX_DOWNLOAD_SIZE
    if sample_needed:
        summary_data["missing-metadata-api-url-label"] += "&sample=100"
    for key, value in summary_data.items():
        if not key.startswith("_"):
            display_element(key, value)
    if sample_needed:
        st.warning(_("missing-metadata-count-too-large-warning"))
        st.warning(_("using-sample-warning"))


def display_download() -> None:
    # Setup the layout so that we can update individual sections
    header_container = st.container(border=True)
    with header_container:
        st.subheader(f"{SERVICE_NAME} {_('download-example-heading')}")

    example_summary_container = st.container(border=True)
    with example_summary_container:
        summary_data = get_summary_data()
        display_summary_container(summary_data)

    retrieve_button_container = st.container(border=True)
    download_status_container = st.container(border=True)
    download_status_container.empty()
    results_container = st.container(border=True)
    download_button_container = st.container(border=True)

    if get_global_session_state("download-status") == "idle":
        with retrieve_button_container:
            st.button(
                _("retrieve-metadata-button-label"),
                on_click=all_results_offset,
                args=(
                    summary_data["missing-metadata-api-url-label"],
                    ROWS_PER_REQUEST,
                    HEADERS,
                    download_status_container,
                ),
                help=_("retrieve-metadata-button-help"),
            )
            display_reports_page_button()
    elif get_global_session_state("download-status") == "complete":
        with results_container:
            st.dataframe(
                summarize_results(get_global_session_state("download-results")),
                use_container_width=True,
                hide_index=True,
                column_config=get_download_example_column_config(),
            )
        with download_button_container:

            st.download_button(
                _("download-metadata-button-label"),
                file_name=download_file_name(summary_data),
                data=str(
                    summarize_results(
                        get_global_session_state("download-results")
                    ).to_csv(index=False)
                ),
                help=_("download-metadata-button-help"),
            )
            display_reports_page_button()


st.set_page_config(
    page_title=f"{CODE_NAME}: Download missing metadata example",
    page_icon=None,
    layout="wide",
    menu_items=None,
)
insert_css()
if "download-status" not in st.session_state:
    set_global_session_state("download-status", "idle")
display_download()
