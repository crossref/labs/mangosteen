MISSING_ITEM_URLS = {

'abstracts': {'filter': 'has-abstract:f'},
'affiliations': {'filter': 'has-affiliation:f'},
'award-numbers': {'filter': 'has-award:f'},
'orcids': {'filter': 'has-orcid:f'},
'licenses': {'filter': 'has-license:f'},
'references' : {'filter': 'has-references:f'},
'funders' : {'filter': 'has-funder:f'},
'similarity-checking' : {'filter' :'has-full-text:f'},
'ror-ids' : {'filter' :'has-ror-id:f'},
'update-policies': {'filter' :'has-update-policy:f'},
'resource-links' : {'filter' :'has-full-text:f'}





# Descriptions is not showing as a valid filter on members route even though in swagger docs
# 'descriptions': {'filter' :'has-description:f'}

}