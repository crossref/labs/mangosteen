#!/usr/bin/env bash
curl -SL "https://api.labs.crossref.org/journals/?mailto=labs@crossref.org&rows=all" --retry 5 -o journals.json