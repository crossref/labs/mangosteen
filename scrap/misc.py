# logging.basicConfig(level=logging.INFO)


# class InvalidURLParameter(Exception):
#     """Exception raised for errors in the format of the URL parameter."""

#     def __init__(self, message="Invalid URL parameter"):
#         self.message = message
#         super().__init__(self.message)


# # def add_query_parameters_to_url(parsed_url, new_params: dict = None):
# #     url = parsed_url.geturl()
# #     req = requests.Request("GET", url, params=new_params)
# #     prepped = req.prepare()
# #     return urlparse(prepped.url)


# def validate_api_url():
#     api_url = st.query_params.get("api-url", {})

#     if not api_url:
#         raise InvalidURLParameter("No URL parameter passed")

#     parsed_url = urlparse(api_url)

#     if not parsed_url.netloc.startswith(
#         "api.crossref.org"
#     ) or not parsed_url.path.endswith("/works"):
#         raise InvalidURLParameter(
#             "URL parameter must be to Crossref API and include the /works route"
#         )

#     return parsed_url


# # def display_description(api_url):
# #     data_label = st.query_params.get("data-label", {})
# #     selected_period = get_global_session_state("selected-period")
# #     if data_label:
# #         st.success(
# #             f"Download {selected_period} metadata records that are missing {_(data_label)}"
# #         )
# #     else:
# #         st.success("Download records for the following API request:")
# #     st.success(f"API URL: {api_url.geturl()}")


# def calculate_request_stats(api_url):
#     population_url = strip_parameters(api_url)
#     missing_coverage_url = add_query_parameters_to_url(
#         api_url, {"rows": ROWS_PER_REQUEST}
#     )
#     with st.spinner("Checking population size...") as spinner:
#         t1 = time()
#         population_size = get_population(population_url)
#         t2 = time()

#     with st.spinner("Getting missing coverage count...") as spinner:
#         t3 = time()
#         missing_coverage_count = get_population(api_url.geturl())
#         t4 = time()
#     return {
#         "missing_coverage_url": add_query_parameters_to_url(
#             missing_coverage_url, {"cursor": "*"}
#         ),
#         "population_size": population_size,
#         "population_size_request_time": t2 - t1,
#         "missing_coverage_count": missing_coverage_count,
#         "missing_coverage_count_request_time": t4 - t3,
#         "percentage_missing": missing_coverage_count / population_size * 100,
#     }


# def calculate_estimated_download_time(request_stats):
#     return (
#         request_stats["missing_metadata_count"]
#         / ROWS_PER_REQUEST
#         * (request_stats["missing_metadata_count_request_time"])
#     )


# def seconds_to_humanized_duration(seconds):
#     return humanize.precisedelta(datetime.timedelta(seconds=seconds))

# # =============================================================================


# def strip_parameters(url):
#     parsed_url = urlparse(url)
#     return f"{parsed_url.scheme}://{parsed_url.netloc}{parsed_url.path}"

# def display_link_home():
#     if st.button(
#         _("return-to-service-button-label"),
#         help=f"{_('return-to-service')} {SERVICE_NAME}",
#     ):
#         st.session_state["download-example"] = None
#         st.switch_page("/app.py")


# def display_example_from_url() -> None:
#     st.write("Example from URL")


# def build_population_query_url_from_streamlit() -> str:
#     member_id = get_global_session_state("download-example")["member-id"]
#     path = f"members/{member_id}/works"
#     content_type = get_global_session_state("download-example")["content-type"]
#     content_type_filter = {"type": content_type}
#     selected_period = get_global_session_state("selected-period")
#     period_filter = rest_api_period_filter(selected_period)
#     crossref_style_filter_parameter = convert_to_crossref_style_filter(
#         period_filter | content_type_filter
#     )
#     prepared_url = requests.Request(
#         method="GET",
#         url=f"{API_URI}/{path}",
#         params=crossref_style_filter_parameter,
#     ).prepare()
#     return prepared_url.url


# def debug_output():
#     pass


# def display_example_from_streamlit() -> None:
#     st.subheader(SERVICE_NAME)
#     # st.write(st.session_state["download-example"])
#     st.subheader(_("download-example-missing-metadata-subheader"))
#     # 1. Build the base URL. We can use this to start estimating time and size
#     missing_metadata_test_url = build_base_query_url_from_streamlit()
#     # st.write(f"Test query URL: {missing_metadata_test_url}")

#     # 2. Remove filters from the URL to get the population size
#     # population_url = strip_parameters(missing_metadata_test_url)
#     # population_url = build_population_query_url_from_streamlit()
#     # st.write(f"Population URL: {population_url}")

#     # 3. Add a sample parameter to the URL
#     sample_url = merge_params(missing_metadata_test_url, {"sample": 100})
#     # st.write(f"Sample  URL: {sample_url}")

#     # 4. Add a cursor parameter to the URL
#     # first_cursor_url = merge_params(missing_metadata_test_url, {"cursor": "*"})
#     # st.write(f"First cursor URL: {first_cursor_url}")
#     # population_request_time, population = get_population(population_url)
#     missing_metadata_request_time, missing_metadata_count = get_population(
#         missing_metadata_test_url
#     )
#     # st.write(f"Population size: {population:,}")
#     with st.container(border=True):
#         member_name = get_global_session_state("download-example")["member-name"]
#         data_label = get_global_session_state("download-example")["data-label"]
#         content_type = get_global_session_state("download-example")["content-type"]
#         selected_period = get_global_session_state("selected-period")
#         st.write(
#             f"<span class='cr-gridify-row-label'>{_('download-example-member-name')}</span>: {member_name}",
#             unsafe_allow_html=True,
#         )
#         st.write(
#             f"<span class='cr-gridify-row-label'>{_('download-example-content-type')}</span>: {content_type}",
#             unsafe_allow_html=True,
#         )
#         st.write(
#             f"<span class='cr-gridify-row-label'>{_('download-example-period')}</span>: {selected_period}",
#             unsafe_allow_html=True,
#         )
#         st.write(
#             f"<span class='cr-gridify-row-label'>{_('download-example-missing-metadata')}</span>: {data_label}",
#             unsafe_allow_html=True,
#         )

#         st.write(
#             f"<span class='cr-gridify-row-label'>{_('missing-metadata-count')}</span>: {missing_metadata_count:,}",
#             unsafe_allow_html=True,
#         )

#     # st.write(st.session_state["download-example"])
#     url_to_use = missing_metadata_test_url
#     request_func = all_results_cursor
#     if missing_metadata_count > MAX_DOWNLOAD_SIZE:
#         st.warning(_("missing-metadata-count-too-large-warning"))
#         url_to_use = sample_url
#         request_func = all_results_offset
#     # elif missing_metadata_count / population > 0.5:
#     #     st.info(_("missing-metadata-count-over-max-percent-info"))

#     # # st.write(f"URL to use: {url_to_use}")
#     # estimated_download_time = calculate_estimated_download_time(
#     #     {
#     #         "missing_metadata_count": missing_metadata_count,
#     #         "missing_metadata_count_request_time": missing_metadata_request_time,
#     #     }
#     # )
#     # st.info(
#     #     f"Estimated download time: {seconds_to_humanized_duration(estimated_download_time)}"
#     # )
#     # st.info(f"Using {request_func.__name__} to retrieve data")
#     if st.button(_("retrieve-metadata-button-label")):
#         retrieve_data(url_to_use, request_func)
#     # display_link_home()


# def invocation_type() -> Literal["url", "streamlit"] | None:
#     if "download-example" in st.session_state:
#         return "streamlit"
#     elif st.query_params.get("api-url"):
#         return "url"
#     return None


# def display_unrecognized_request() -> None:
#     st.error(_("unrecognized-request"))


# # Main

# st.title("Download Examples")
# insert_css()
# case = invocation_type()
# if case == "url":
#     display_example_from_url()
# elif case == "streamlit":
#     display_example_from_streamlit()
# else:
#     display_unrecognized_request()


def display_download():
    # Setup the layout so that we can update individual sections
    header_container = st.container(border=True)
    with header_container:
        st.subheader(f"{SERVICE_NAME} Download Example")

    example_summary_container = st.container(border=True)
    with example_summary_container:
        summary_data = get_summary_data()
        display_summary_container(summary_data)

    # If the page has just been loaded, the download status will be blank
    # if st.session_state["download-status"] == "idle":
    #     download_status_container = st.container(border=True).empty()

    download_status_container = st.container(border=True).empty()

    retrieve_button_container = st.container(border=True)
    results_container = st.container(border=True)
    download_button_container = st.container(border=True)

    if st.session_state["download-status"] == "idle":
        with retrieve_button_container:
            # st.button(
            #     "Retrieve",
            #     on_click=fake_get_results,
            #     args=(
            #         summary_data["missing-metadata-api-url"],
            #         download_status_container,
            #     ),
            # )

            st.button(
                "Retrieve",
                on_click=all_results_offset,
                args=(URL, ROWS_PER_REQUEST, HEADERS, download_status_container),
            )
            display_reports_page_button()
    elif st.session_state["download-status"] == "complete":
        with results_container:
            # st.write("Results = [1, 2, 3]")
            # st.write(f"{len(st.session_state["download-results"])} records")
            st.write(summarize_results(st.session_state["download-results"]))
        with download_button_container:
            st.button("Download Results")
            display_reports_page_button()
