import streamlit as st
import requests


from urllib.parse import unquote
import json

from time import time
from urllib.parse import urlparse, parse_qs, urlencode
from mangosteen.settings import API_URI, SERVICE_NAME
from mangosteen.data import lookup_filter, rest_api_period_filter
from mangosteen.sessions import get_global_session_state, cancel_download_examples

from mangosteen.l10n import _


ROWS_PER_REQUEST = 500
MAX_DOWNLOAD_SIZE = 5000


def get_population(population_url: str):
    t1 = time()
    total = requests.get(population_url).json()["message"]["total-results"]
    return time() - t1, int(total)


def all_results_cursor(url, rows=ROWS_PER_REQUEST, headers=None, context=None):
    params = {"cursor": "*", "rows": rows}
    items = []
    if not context:
        context = st
    with requests.Session() as s:
        while True:
            # print(f">>>>>>>>>>>>{url}")
            prepared = requests.Request(
                method="GET", url=url, headers=headers, params=params
            ).prepare()

            res = s.send(prepared)
            if res.status_code != 200:
                raise ConnectionError(f"API returned code {res.code}")

            record = res.json()

            # are we done?
            if not record["message"]["items"]:
                return items
            # more to go
            items += record["message"]["items"]
            context.write(
                f"{_('records-retrieved')} : {len(items):,} / {record['message']['total-results']:,}"
            )
            params["cursor"] = record["message"]["next-cursor"]


def all_results_offset(url, rows=ROWS_PER_REQUEST, headers=None, context=None):
    params = {"offset": 0, "rows": rows}
    items = []
    if not context:
        context = st
    with requests.Session() as s:
        context.write(f"{_('retrieving-sample-of-size-message')}: {MAX_DOWNLOAD_SIZE}")
        while True:

            prepared = requests.Request(
                method="GET", url=url, headers=headers
            ).prepare()

            res = s.send(prepared)
            if res.status_code != 200:
                raise ConnectionError(f"API returned code {res.code}")

            record = res.json()

            items += record["message"]["items"]
            context.write(
                f"{_('records-retrieved')} : {len(items):,} / {MAX_DOWNLOAD_SIZE:,}"
            )

            # are we done?
            if len(items) >= MAX_DOWNLOAD_SIZE:
                context.write(
                    f"Finished: retrieved {len(items)} of {MAX_DOWNLOAD_SIZE}"
                )
                return items
            # more to go
            params["offset"] += rows


def retrieve_data(url, request_func, context=None):

    if not context:
        context = st

    with context.status(
        _("retrieving-metadata-status"), expanded=True, state="running"
    ) as status:
        t1 = time()
        context.write(f"{_('retrieving-metadata-from-url')}: {unquote(url)}")

        data = request_func(
            url, headers={"Accept": "application/json"}, context=context
        )
        context.write(
            f"{_('time-in-seconds-to-retrieve-metadata')} : {time() - t1:.2f}"
        )
        status.update(
            label=_("example-metadata-retrieved-completed"),
            state="complete",
            expanded=False,
        )

    context.download_button(
        _("download-metadata-button-label"),
        json.dumps(data),
        "data.json",
        "application/json",
    )


def merge_params(url, new_params):
    parsed_url = urlparse(url)
    query = parse_qs(parsed_url.query)
    query.update(new_params)
    new_query = urlencode(query, doseq=True)
    return parsed_url._replace(query=new_query).geturl()


def convert_to_crossref_style_filter(filters):
    value = ",".join([f"{k}:{v}" for k, v in filters.items()])
    return {"filter": value}


def build_base_query_url_from_streamlit() -> str:

    member_id = get_global_session_state("download-example")["member-id"]
    path = f"members/{member_id}/works"

    content_type = get_global_session_state("download-example")["content-type"]
    content_type_filter = {"type": content_type}

    selected_period = get_global_session_state("selected-period")
    period_filter = rest_api_period_filter(selected_period)

    missing_metadata_filter_key = get_global_session_state("download-example")[
        "filter-key"
    ]
    missing_metadata_filter = lookup_filter(missing_metadata_filter_key)

    combined_filters = missing_metadata_filter | period_filter | content_type_filter
    crossref_style_filter_parameter = convert_to_crossref_style_filter(combined_filters)

    prepared_url = requests.Request(
        method="GET",
        url=f"{API_URI}/{path}",
        params=crossref_style_filter_parameter,
    ).prepare()
    return prepared_url.url


def display_element(label: str, text: str):
    st.write(
        f"<span class='cr-gridify-row-label'>{_(label)}</span>: {text}",
        unsafe_allow_html=True,
    )


def get_summary_data():
    member_name = get_global_session_state("download-example")["member-name"]
    data_label = get_global_session_state("download-example")["data-label"]
    content_type = get_global_session_state("download-example")["content-type"]
    selected_period = get_global_session_state("selected-period")

    missing_metadata_test_url = build_base_query_url_from_streamlit()
    missing_metadata_request_time, missing_metadata_count = get_population(
        missing_metadata_test_url
    )
    sample_needed = missing_metadata_count > MAX_DOWNLOAD_SIZE
    if sample_needed:
        missing_metadata_test_url + f"&sample=100"

    return {
        "download-example-member-name-label": member_name,
        "download-example-content-type-label": content_type,
        "download-example-missing-metadata-label": data_label,
        "download-example-period-label": selected_period,
        "missing-metadata-count-label": missing_metadata_count,
        "missing-metadata-api-url-label": missing_metadata_test_url,
        "_request-time": missing_metadata_request_time,
        "_sample-needed": sample_needed,
    }


def display_download_examples():

    header_container = st.container(border=True)
    summary_container = st.container(border=True)
    download_status_container = st.container(border=True)
    button_row_container = st.container(border=True)

    with header_container:
        st.markdown(f"<!-- {SERVICE_NAME}-download-examples-->", unsafe_allow_html=True)
        st.subheader(SERVICE_NAME)
        st.subheader(_("download-example-missing-metadata-subheader"))

    # 1. Build the base URL. We can use this to start estimating time and size
    missing_metadata_test_url = build_base_query_url_from_streamlit()

    #     # 2. Remove filters from the URL to get the population size
    #     # population_url = strip_parameters(missing_metadata_test_url)
    #     # population_url = build_population_query_url_from_streamlit()
    #     # st.write(f"Population URL: {population_url}")

    # 3. Add a sample parameter to the URL
    sample_url = merge_params(missing_metadata_test_url, {"sample": 100})
    # st.write(f"Sample  URL: {sample_url}")

    #     # 4. Add a cursor parameter to the URL
    #     # first_cursor_url = merge_params(missing_metadata_test_url, {"cursor": "*"})
    #     # st.write(f"First cursor URL: {first_cursor_url}")
    #     # population_request_time, population = get_population(population_url)
    missing_metadata_request_time, missing_metadata_count = get_population(
        missing_metadata_test_url
    )

    with summary_container:
        member_name = get_global_session_state("download-example")["member-name"]
        data_label = get_global_session_state("download-example")["data-label"]
        content_type = get_global_session_state("download-example")["content-type"]
        selected_period = get_global_session_state("selected-period")

        display_element("member-name-label", member_name)
        display_element("content-type-label", content_type)
        display_element("selected-period-label", selected_period)
        display_element("data-label", data_label)
        display_element("missing-metadata-count-label", f"{missing_metadata_count:,}")
        display_element("url-label", missing_metadata_test_url)

        url_to_use = missing_metadata_test_url
        request_func = all_results_cursor
        if missing_metadata_count > MAX_DOWNLOAD_SIZE:
            st.warning(_("missing-metadata-count-too-large-warning"))
            url_to_use = sample_url
            request_func = all_results_offset

    with download_status_container:
        # status_bar = st.status("Metadata retrieval status: 0%", state="complete")
        st.progress(0, text="Data not yet retrieved")

    with button_row_container:
        columns = st.columns([1, 1])
        columns[0].button(
            _("retrieve-metadata-button-label"),
            on_click=retrieve_data,
            args=(url_to_use, request_func, download_status_container),
            key="retrieve-metadata-button",
        )
        columns[1].button(
            _("cancel-download-examples-button"),
            on_click=cancel_download_examples,
            key="cancel-download-examples-button",
        )
